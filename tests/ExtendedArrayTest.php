<?php

namespace Tests\JumpIfBelow\Arrays;

use JumpIfBelow\Arrays\Exception\BadOffsetException;
use JumpIfBelow\Arrays\Exception\BadQuantileDistributionException;
use JumpIfBelow\Arrays\Exception\ElementNotFoundException;
use JumpIfBelow\Arrays\Entry;
use JumpIfBelow\Arrays\ExtendedArray;
use JumpIfBelow\Arrays\ExtendedArrayInterface;
use PHPUnit\Framework\TestCase;

class ExtendedArrayTest extends TestCase
{
    public function testForEach(): void
    {
        $arr = ExtendedArray::from([5 => 'first', 3 => 'second', 'associative' => 'third']);
        $copy = clone $arr;

        $iteration = 0;
        $expected = [[5, 'first'], [3, 'second'], ['associative', 'third']];

        $result = $arr->forEach(function ($item, $key, $self) use (&$iteration, $expected, $arr) {
            $element = $expected[$iteration++];

            $this->assertEquals($element[1], $item);
            $this->assertEquals($element[0], $key);
            $this->assertEquals($arr, $self);
        });

        $this->assertEquals($copy, $result);
    }

    public function testEvery(): void
    {
        $arr = ExtendedArray::range(0, 9);

        $this->assertTrue($arr->every('is_int'));
        $this->assertFalse($arr->every(fn(int $value): bool => $value % 2 === 0));
        $this->assertFalse($arr->every('is_string'));
    }

    public function testSome(): void
    {
        $arr = ExtendedArray::range(0, 9);

        $this->assertTrue($arr->some('is_int'));
        $this->assertTrue($arr->some(fn(int $value): bool => $value % 2 === 0));
        $this->assertFalse($arr->some('is_string'));
    }

    public function testEntry(): void
    {
        $arr = ExtendedArray::range(0, 9);

        $this->assertEquals(new Entry(0, 0), $arr->entry());

        $arr->next();
        $this->assertEquals(new Entry(1, 1), $arr->entry());

        $arr->end();
        $this->assertEquals(new Entry(9, 9), $arr->entry());

        $arr->next();
        $this->assertNull($arr->entry());

        $arr->rewind();
        $this->assertEquals(new Entry(0, 0), $arr->entry());
    }

    public function testNextKey(): void
    {
        $arr = ExtendedArray::range(0, 9);

        $this->assertEquals(1, $arr->nextKey());
        $this->assertEquals(2, $arr->nextKey());

        $arr->end();
        $this->assertNull($arr->nextKey());

        $arr->rewind();
        $this->assertEquals(1, $arr->nextKey());
    }

    public function testNextValue(): void
    {
        $arr = ExtendedArray::range(0, 9);

        $this->assertEquals(1, $arr->nextValue());
        $this->assertEquals(2, $arr->nextValue());

        $arr->end();
        $this->assertNull($arr->nextValue());

        $arr->rewind();
        $this->assertEquals(1, $arr->nextValue());
    }

    public function testNextEntry(): void
    {

        $arr = ExtendedArray::range(0, 9);

        $this->assertEquals(new Entry(1, 1), $arr->nextEntry());
        $this->assertEquals(new Entry(2, 2), $arr->nextEntry());

        $arr->end();
        $this->assertNull($arr->nextEntry());

        $arr->rewind();
        $this->assertEquals(new Entry(1, 1), $arr->nextEntry());
    }

    public function testFindKey(): void
    {
        $arr1 = ExtendedArray::range(0, 9);
        $arr2 = ExtendedArray::from([3, 4, 's', 5, 't', 'r']);

        $this->assertEquals(0, $arr1->findKey(fn($value) => $value % 2 === 0));
        $this->assertEquals(2, $arr2->findKey('is_string'));

        $this->expectExceptionObject(new ElementNotFoundException());
        $arr1->findValue('is_string');
    }

    public function testFindValue(): void
    {
        $arr1 = ExtendedArray::range(0, 9);
        $arr2 = ExtendedArray::from([3, 4, 's', 5, 't', 'r']);

        $this->assertEquals(0, $arr1->findValue(fn($value) => $value % 2 === 0));
        $this->assertEquals('s', $arr2->findValue('is_string'));

        $this->expectExceptionObject(new ElementNotFoundException());
        $arr1->findValue('is_string');
    }

    public function testKeyOf(): void
    {
        $arr = ExtendedArray::from(['er', 'eer', 'er', true, 'i' => false]);

        $this->assertEquals(0, $arr->keyOf('er'));
        $this->assertEquals(1, $arr->keyOf('eer'));
        $this->assertEquals('i', $arr->keyOf(false));

        $this->expectExceptionObject(new ElementNotFoundException());
        $arr->keyOf('j');
    }

    public function testRemoveKey(): void
    {
        $arr = ExtendedArray::from(['er', 'eer', 'er', true, 'i' => false]);
        $expected = ExtendedArray::from(['er', 'eer', 3 => true, 'i' => false]);

        $this->assertEquals($expected, $arr->removeKey(2));
        $this->assertNotSame($arr, $arr->removeKey('i'));

        $this->expectExceptionObject(new BadOffsetException());
        $arr->removeKey('j');
    }

    public function testRemoveValue(): void
    {
        $arr = ExtendedArray::from(['er', 'eer', 'er', true, 'i' => false]);
        $expected = ExtendedArray::from([1 => 'eer', 2 => 'er', 3 => true, 'i' => false]);

        $this->assertEquals($expected, $arr->removeValue('er'));
        $this->assertNotSame($arr, $arr->removeValue(false));

        $this->expectExceptionObject(new ElementNotFoundException());
        $arr->removeValue(null);
    }

    public function testAverage(): void
    {
        $this->assertEquals(1, ExtendedArray::range(0, 2)->average());
        $this->assertEquals(10, ExtendedArray::from([10, 5, 15, 7.5, 12.5])->average());
    }

    public function testVariance(): void
    {
        $this->assertEquals(2 / 3, ExtendedArray::from([1, 2, 3])->variance());
        $this->assertEquals(21_704, ExtendedArray::from([600, 470, 170, 430, 300])->variance());
    }

    public function testStandardDeviation(): void
    {
        $arr1 = ExtendedArray::from([1, 2, 3]);
        $arr2 = ExtendedArray::from([600, 470, 170, 430, 300]);

        $this->assertEquals(sqrt($arr1->variance()), $arr1->standardDeviation());
        $this->assertEquals(sqrt($arr2->variance()), $arr2->standardDeviation());
    }

    public function testQuantiles(): void
    {
        $extendedArray = ExtendedArray::range(0, 9);

        $this->assertEquals(ExtendedArray::from([5]), $extendedArray->quantiles(1 / 2));
        $this->assertEquals(ExtendedArray::from([4, 7]), $extendedArray->quantiles(1 / 3));
        $this->assertEquals(ExtendedArray::from([3, 5, 8]), $extendedArray->quantiles(1 / 4));
        $this->assertEquals(ExtendedArray::from([2, 4, 6, 8]), $extendedArray->quantiles(1 / 5));

        $this->expectExceptionObject(new BadQuantileDistributionException());
        $extendedArray->quantiles(0.7);
    }

    public function testQuantileKeys(): void
    {
        $extendedArray = ExtendedArray::range(0, 9);

        $this->assertEquals(ExtendedArray::from([5]), $extendedArray->quantileKeys(1 / 2));
        $this->assertEquals(ExtendedArray::from([4, 7]), $extendedArray->quantileKeys(1 / 3));
        $this->assertEquals(ExtendedArray::from([3, 5, 8]), $extendedArray->quantileKeys(1 / 4));
        $this->assertEquals(ExtendedArray::from([2, 4, 6, 8]), $extendedArray->quantileKeys(1 / 5));

        $this->expectExceptionObject(new BadQuantileDistributionException());
        $extendedArray->quantileKeys(0.7);
    }

    public function testQuantileValues(): void
    {
        $extendedArray = ExtendedArray::range(0, 9);

        $this->assertEquals(ExtendedArray::from([[0, 1, 2, 3, 4], [5, 6, 7, 8, 9]]), $extendedArray->quantileValues(1 / 2));
        $this->assertEquals(ExtendedArray::from([[0, 1, 2, 3], [4, 5, 6], [7, 8, 9]]), $extendedArray->quantileValues(1 / 3));
        $this->assertEquals(ExtendedArray::from([[0, 1, 2], [3, 4], [5, 6, 7], [8, 9]]), $extendedArray->quantileValues(1 / 4));
        $this->assertEquals(ExtendedArray::from([[0, 1], [2, 3], [4, 5], [6, 7], [8, 9]]), $extendedArray->quantileValues(1 / 5));

        $this->expectExceptionObject(new BadQuantileDistributionException());
        $extendedArray->quantiles(0.7);
    }

    public function testFlat(): void
    {
        $arr = ExtendedArray::from([
            0,
            1,
            [
                2,
                3,
                [
                    4,
                    5,
                    [
                        6,
                        7,
                        [
                            8,
                            9,
                        ],
                    ],
                ],
            ],
            [
                10,
                11,
                [
                    12,
                    13,
                ],
            ],
        ]);

        $this->assertEquals(
            ExtendedArray::range(0, 13),
            $arr->flat()
        );

        $this->assertEquals(
            ExtendedArray::from([
                0,
                1,
                2,
                3,
                [
                    4,
                    5,
                    [
                        6,
                        7,
                        [
                            8,
                            9,
                        ],
                    ],
                ],
                10,
                11,
                [
                    12,
                    13
,                ]
            ]),
            $arr->flat(1)
        );

        $this->assertEquals(
            ExtendedArray::from([
                0,
                1,
                2,
                3,
                4,
                5,
                [
                    6,
                    7,
                    [
                        8,
                        9,
                    ],
                ],
                10,
                11,
                12,
                13,
            ]),
            $arr->flat(2)
        );

        $this->assertEquals(
            ExtendedArray::from([
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                [
                    8,
                    9,
                ],
                10,
                11,
                12,
                13,
            ]),

            $arr->flat(3)
        );

        $this->assertEquals(
            ExtendedArray::range(0, 13),
            $arr->flat(4)
        );

        $this->assertEquals(
            ExtendedArray::range(0, 13),
            $arr->flat(100)
        );
    }

    public function testGroupBy(): void
    {
        $arr1 = ExtendedArray::range(0, 9);
        $arr2 = ExtendedArray::from(['s', 4, 5, 't', 'r', 6]);

        $this->assertEquals(
            ExtendedArray::from([[0, 2, 4, 6, 8], [1, 3, 5, 7, 9]]),
            $arr1->groupBy(fn($value): int => $value % 2)
        );

        $this->assertEquals(
            ExtendedArray::from([[4, 5, 6], ['s', 't', 'r']]),
            $arr2->groupBy(fn($value): int => is_string($value)),
        );
    }

    public function testIndexBy(): void
    {
        $arr = ExtendedArray::from([
            ['id' => 5, 'value' => 4],
            ['id' => 56, 'value' => false],
            ['id' => 'str', 'value' => []],
        ]);
        $clone = clone $arr;

        $expected = ExtendedArray::from([
            5 => ['id' => 5, 'value' => 4],
            56 => ['id' => 56, 'value' => false],
            'str' => ['id' => 'str', 'value' => []],
        ]);

        $indexer = fn (ExtendedArrayInterface $extendedArray) => $extendedArray['id'];

        $this->assertEquals($expected, $arr->indexBy($indexer));
        $this->assertNotSame($clone, $arr->indexBy($indexer));
    }

    public function testFirst(): void
    {
        $arr = ExtendedArray::range(0, 9);

        $this->assertEquals(0, $arr->first());
        unset($arr[$arr->keyFirst()]);
        $this->assertEquals(1, $arr->first());

        $this->expectExceptionObject(new BadOffsetException());
        ExtendedArray
            ::from()
            ->first()
        ;
    }

    public function testLast(): void
    {
        $arr = ExtendedArray::range(0, 9);

        $this->assertEquals(9, $arr->last());
        unset($arr[$arr->keyLast()]);
        $this->assertEquals(8, $arr->last());

        $this->expectExceptionObject(new BadOffsetException());
        ExtendedArray
            ::from()
            ->last()
        ;
    }

    public function testPermutation(): void
    {
        $arr = ExtendedArray::from([1, 2, 3]);
        $full = ExtendedArray::from([
            [1, 2, 3],
            [1, 3, 2],
            [2, 1, 3],
            [2, 3, 1],
            [3, 1, 2],
            [3, 2, 1],
        ]);
        /**
         * @todo future tests when permutation works as Ruby
         * @see https://en.wikipedia.org/wiki/Heap's_algorithm#Details_of_the_algorithm
         */
//        $one = ExtendedArray::from([[1], [2], [3]]);
//        $two = ExtendedArray::from([
//            [1, 2],
//            [1, 3],
//            [2, 1],
//            [2, 3],
//            [3, 1],
//            [3, 2],
//        ]);
//        $three = clone $full;
//        $zero = ExtendedArray::from([[]]);
//        $four = ExtendedArray::from([]);

        $this->assertEquals($full, $arr->permutation());
//        $this->assertEquals($one, $arr->permutation(1));
//        $this->assertEquals($two, $arr->permutation(2));
//        $this->assertEquals($three, $arr->permutation(3));
//        $this->assertEquals($zero, $arr->permutation(0));
//        $this->assertEquals($four, $arr->permutation(4));
    }

    public function testFullMap(): void
    {
        $arr = ExtendedArray::range('A', 'J')
            ->combine(ExtendedArray::range(0, 9))
        ;

        $this->assertEquals(
            ExtendedArray::range('A', 'J')->combine(ExtendedArray::from([0, 2, 4, 6, 8, 10, 12, 14, 16, 18])),
            $arr->fullMap(fn($value) => $value * 2)
        );

        $this->assertEquals(
            ExtendedArray::range('A', 'J')->combine(ExtendedArray::range('A', 'J')),
            $arr->fullMap(fn($value, $key) => $key)
        );

        $this->assertEquals(
            ExtendedArray::range('A', 'J')->combine(ExtendedArray::fill(0, 10, 0)),
            $arr->fullMap(fn($value, $key, $thisArg) => $thisArg['A'])
        );
    }

    public function testFullReduce(): void
    {
        $arr = ExtendedArray::range(0 ,9);

        $this->assertEquals(45, $arr->fullReduce(fn($carry, $item) => $carry + $item));
        $this->assertEquals(0, $arr->fullReduce(fn($carry, $item) => $carry * $item));
        $this->assertEquals(47, $arr->fullReduce(fn($carry, $item) => $carry + $item, 2));
        $this->assertEquals(285, $arr->fullReduce(fn($carry, $item, $key) => $carry + $item * $key, 0));
        $this->assertEquals(287, $arr->fullReduce(fn($carry, $item, $key) => $carry + $item * $key, 2));
    }

    public function testFetch(): void
    {
        $arr = ExtendedArray::from([
            'a' => 0,
            'b' => true,
            'c' => null,
            'd' => 'd',
        ]);

        $this->assertEquals(0, $arr->fetch(0));
        $this->assertEquals(true, $arr->fetch(1));
        $this->assertEquals(null, $arr->fetch(2));
        $this->assertEquals('d', $arr->fetch(3));

        $this->assertEquals('d', $arr->fetch(-1));
        $this->assertEquals(null, $arr->fetch(-2));
        $this->assertEquals(true, $arr->fetch(-3));
        $this->assertEquals(0, $arr->fetch(-4));
    }

    public function testSwap(): void
    {
        $arr = ExtendedArray::range(0, 9);

        $this->assertNotSame($arr->swap(0, 9), $arr->swap(0, 9));

        $this->assertEquals(
            ExtendedArray::from([9, 1, 2, 3, 4, 5, 6, 7, 8, 0]),
            $arr->swap(0, 9)
        );

        $this->assertEquals(
            ExtendedArray::from([0, 1, 2, 3, 4, 6, 5, 7, 8, 9]),
            $arr->swap(5, 6)
        );

        $this->assertEquals($arr->swap(1, 3), $arr->swap(3, 1));
    }

    public function testWith(): void
    {
        $arr = ExtendedArray::from([true]);

        $this->assertNotSame($arr, $arr->with(1, null));
        $this->assertEquals(ExtendedArray::from([true, 1]), $arr->with(1, 1));
        $this->assertNotSame($arr->with(0, 'set'), $arr->with(0, 'set'));
        $this->assertEquals($arr->with(0, 'set'), $arr->with(0, 'set'));
    }

    public function testCartesian(): void
    {
        $arr1 = ExtendedArray::from([1, 2, 3]);
        $arr2 = ExtendedArray::from([4, 5, 6]);
        $arr3 = ExtendedArray::from([7, 8, 9, 10]);
        $arr4 = ExtendedArray::from([[1, 2], [3, 4], [5, 6]]);

        $this->assertEquals(
            $arr1->cartesian($arr2),
            ExtendedArray::from([
                [1, 4],
                [1, 5],
                [1, 6],
                [2, 4],
                [2, 5],
                [2, 6],
                [3, 4],
                [3, 5],
                [3, 6],
            ]),
        );

        $this->assertEquals(
            $arr1->cartesian($arr3),
            ExtendedArray::from([
                [1, 7],
                [1, 8],
                [1, 9],
                [1, 10],
                [2, 7],
                [2, 8],
                [2, 9],
                [2, 10],
                [3, 7],
                [3, 8],
                [3, 9],
                [3, 10],
            ]),
        );

        $this->assertEquals(
            $arr1->cartesian($arr2, $arr3),
            ExtendedArray::from([
                [1, 4, 7],
                [1, 4, 8],
                [1, 4, 9],
                [1, 4, 10],
                [1, 5, 7],
                [1, 5, 8],
                [1, 5, 9],
                [1, 5, 10],
                [1, 6, 7],
                [1, 6, 8],
                [1, 6, 9],
                [1, 6, 10],
                [2, 4, 7],
                [2, 4, 8],
                [2, 4, 9],
                [2, 4, 10],
                [2, 5, 7],
                [2, 5, 8],
                [2, 5, 9],
                [2, 5, 10],
                [2, 6, 7],
                [2, 6, 8],
                [2, 6, 9],
                [2, 6, 10],
                [3, 4, 7],
                [3, 4, 8],
                [3, 4, 9],
                [3, 4, 10],
                [3, 5, 7],
                [3, 5, 8],
                [3, 5, 9],
                [3, 5, 10],
                [3, 6, 7],
                [3, 6, 8],
                [3, 6, 9],
                [3, 6, 10],
            ]),
        );

        $this->assertEquals(
            $arr4->cartesian($arr1),
            ExtendedArray::from([
                [[1, 2], 1],
                [[1, 2], 2],
                [[1, 2], 3],
                [[3, 4], 1],
                [[3, 4], 2],
                [[3, 4], 3],
                [[5, 6], 1],
                [[5, 6], 2],
                [[5, 6], 3],
            ]),
        );
    }
}
