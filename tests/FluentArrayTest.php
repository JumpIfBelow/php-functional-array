<?php

namespace Tests\JumpIfBelow\Arrays;

use JumpIfBelow\Arrays\Exception\BadOffsetException;
use JumpIfBelow\Arrays\FluentArray;
use PHPUnit\Framework\TestCase;

class FluentArrayTest extends TestCase
{
    public function testFrom(): void
    {
        $inputArray = [0, 1, true, null, 'test'];

        $new = FluentArray::from($inputArray);
        $init = FluentArray::from($inputArray);

        $this->assertEquals($new, $init);

        $recursive = [
            0,
            1,
            &$recursive,
        ];

        $recursive = FluentArray::from($recursive);
        $this->assertSame($recursive, $recursive[2]);

        $recursive = [
            0,
            1,
            [
                2,
                &$recursive,
            ],
        ];

        $recursive = FluentArray::from($recursive);
        $this->assertSame($recursive, $recursive[2][1]);
    }

    public function testClone(): void
    {
        $arr = FluentArray::from();
        $cloned = clone $arr;

        $this->assertEquals($arr, $cloned);
        $this->assertNotSame($arr, $cloned);

        $arr = FluentArray::from([[]]);
        $cloned = clone $arr;

        $this->assertEquals($arr[0], $cloned[0]);
        $this->assertNotSame($arr[0], $cloned[0]);
    }

    public function testExplode(): void
    {
        $arr = FluentArray::explode(',', 'some,value');
        $expected = FluentArray::from(['some', 'value']);

        $this->assertEquals($expected, $arr);
    }

    public function testPregSplit(): void
    {
        $arr = FluentArray::pregSplit('/(,|;)/', 'some,values;separated with, several; delimiters');
        $expected = FluentArray::from(['some', 'values', 'separated with', ' several', ' delimiters']);

        $this->assertEquals($expected, $arr);
    }

    public function testFill(): void
    {
        $arr = FluentArray::fill(0, 5, 'a');
        $expected = FluentArray::from(['a', 'a', 'a', 'a', 'a']);

        $this->assertEquals($expected, $arr);
    }

    public function testRange(): void
    {
        $arr = FluentArray::range('a', 'e');
        $expected = FluentArray::from(['a', 'b', 'c', 'd', 'e']);

        $this->assertEquals($expected, $arr);

        $arr = FluentArray::range(5, 10);
        $expected = FluentArray::from([5, 6, 7, 8, 9, 10]);

        $this->assertEquals($expected, $arr);

        $arr = FluentArray::range(2, 8, 3);
        $expected = FluentArray::from([2, 5, 8]);

        $this->assertEquals($expected, $arr);

        $arr = FluentArray::range(5, 0, 2);
        $expected = FluentArray::from([5, 3, 1]);

        $this->assertEquals($expected, $arr);

        $arr = FluentArray::range('h', 'a', 3);
        $expected = FluentArray::from(['h', 'e', 'b']);

        $this->assertEquals($expected, $arr);
    }

    public function testValid(): void
    {
        $arr = FluentArray::from();
        $this->assertFalse($arr->valid());

        $arr[] = 0;
        $this->assertTrue($arr->valid());

        $arr->next();
        $this->assertFalse($arr->valid());
    }

    public function testRewind(): void
    {
        $arr = FluentArray::from([0, 1]);

        $arr->rewind();
        $arr->next();
        $this->assertEquals(1, $arr->current());

        $arr->rewind();
        $this->assertEquals(0, $arr->current());
    }

    public function testOffsetExists(): void
    {
        $arr = FluentArray::from([0 => 0, 'associative' => 1]);

        $this->assertTrue($arr->offsetExists(0));
        $this->assertTrue($arr->offsetExists('associative'));
        $this->assertFalse($arr->offsetExists('not set'));
    }

    public function testOffsetGet(): void
    {
        $arr = FluentArray::from([0, 'associative' => 1]);

        $this->assertEquals(0, $arr->offsetGet(0));
        $this->assertEquals(1, $arr->offsetGet('associative'));

        $this->expectExceptionObject(new BadOffsetException('The offset ["not set"] does not exists on this array.'));
        $arr->offsetGet('not set');
    }

    public function testOffsetSet(): void
    {
        $arr = FluentArray::from();

        $arr->offsetSet(null, 0);
        $this->assertEquals(0, $arr[0]);

        $arr->offsetSet(null, 5);
        $this->assertEquals(5, $arr[1]);

        $arr->offsetSet(0, 10);
        $this->assertEquals(10, $arr[0]);

        $arr->offsetSet('associative', 3);
        $this->assertEquals(3, $arr['associative']);

        $arr = FluentArray::from([], 2);
        $arr[0] = [];
        $arr[0][0] = [];
        $arr[0][0][0] = [[]];

        $this->assertInstanceOf(FluentArray::class, $arr);
        $this->assertInstanceOf(FluentArray::class, $arr[0]);
        $this->assertInstanceOf(FluentArray::class, $arr[0][0]);
        $this->assertIsArray($arr[0][0][0]);
        $this->assertIsArray($arr[0][0][0][0]);
    }

    public function testOffsetUnset(): void
    {
        $arr = FluentArray::from([0 => 0, 1 => 1, 'associative' => 3]);

        $arr->offsetUnset(0);
        $this->assertFalse($arr->offsetExists(0));
        $this->assertTrue($arr->offsetExists(1));
        $this->assertTrue($arr->offsetExists('associative'));

        $arr->offsetUnset(1);
        $this->assertFalse($arr->offsetExists(0));
        $this->assertFalse($arr->offsetExists(1));
        $this->assertTrue($arr->offsetExists('associative'));

        $arr->offsetUnset('associative');
        $this->assertFalse($arr->offsetExists(0));
        $this->assertFalse($arr->offsetExists(1));
        $this->assertFalse($arr->offsetExists('associative'));
    }

    public function testArsort(): void
    {
        $arr = FluentArray::from(['d' => 'lemon', 'a' => 'orange', 'b' => 'banana', 'c' => 'apple']);
        $copy = clone $arr;
        $expected = FluentArray::from(['a' => 'orange', 'd' => 'lemon', 'b' => 'banana', 'c' => 'apple']);

        $sorted = $arr->asort();

        $this->assertEquals($expected, $sorted);
        $this->assertEquals($copy, $arr);
    }

    public function testAsort(): void
    {
        $arr = FluentArray::from(['d' => 'lemon', 'a' => 'orange', 'b' => 'banana', 'c' => 'apple']);
        $copy = clone $arr;
        $expected = FluentArray::from(['c' => 'apple', 'b' => 'banana', 'd' => 'lemon', 'a' => 'orange']);

        $sorted = $arr->asort();

        $this->assertEquals($expected, $sorted);
        $this->assertEquals($copy, $arr);
    }

    public function testChangeKeyCase(): void
    {
        $arr = FluentArray::from(['FirSt' => 1, 'SecOnd' => 4]);
        $copy = clone $arr;
        $expected = FluentArray::from(['FIRST' => 1, 'SECOND' => 4]);

        $changed = $arr->changeKeyCase(CASE_UPPER);

        $this->assertEquals($expected, $changed);
        $this->assertEquals($copy, $arr);

        $expected = FluentArray::from(['first' => 1, 'second' => 4]);

        $changed = $arr->changeKeyCase();

        $this->assertEquals($expected, $changed);
        $this->assertEquals($copy, $arr);
    }

    public function testChunk(): void
    {
        $arr = FluentArray::from(['a', 'b', 'c', 'd', 'e']);
        $copy = clone $arr;
        $expected = FluentArray::from([
            ['a', 'b'],
            ['c', 'd'],
            ['e'],
        ]);

        $chunk = $arr->chunk(2);

        $this->assertEquals($expected, $chunk);
        $this->assertEquals($copy, $arr);

        $expected = FluentArray::from([
            [0 => 'a', 1 => 'b'],
            [2 => 'c', 3 => 'd'],
            [4 => 'e'],
        ]);

        $chunk = $arr->chunk(2, true);

        $this->assertEquals($expected, $chunk);
        $this->assertEquals($copy, $arr);
    }

    public function testColumn(): void
    {
        $arr = FluentArray::from([
            [
                'id' => 2135,
                'first_name' => 'John',
                'last_name' => 'Doe',
            ],
            [
                'id' => 3245,
                'first_name' => 'Sally',
                'last_name' => 'Smith',
            ],
            [
                'id' => 5342,
                'first_name' => 'Jane',
                'last_name' => 'Jones',
            ],
            [
                'id' => 5623,
                'first_name' => 'Peter',
                'last_name' => 'Doe',
            ],
        ]);

        $copy = clone $arr;
        $expected = FluentArray::from([
            0 => 'John',
            1 => 'Sally',
            2 => 'Jane',
            3 => 'Peter',
        ]);

        $column = $arr->column('first_name');

        $this->assertEquals($expected, $column);
        $this->assertEquals($copy, $arr);

        $expected = FluentArray::from([
            2135 => 'John',
            3245 => 'Sally',
            5342 => 'Jane',
            5623 => 'Peter',
        ]);

        $column = $arr->column('first_name', 'id');

        $this->assertEquals($expected, $column);
        $this->assertEquals($copy, $arr);
    }

    public function testCombine(): void
    {
        $keys = FluentArray::from(['green', 'red', 'yellow']);
        $values = FluentArray::from(['avocado', 'apple', 'banana']);
        $keysCopy = clone $keys;
        $valuesCopy = clone $values;

        $expected = FluentArray::from([
            'green' => 'avocado',
            'red' => 'apple',
            'yellow' => 'banana',
        ]);

        $combined = $keys->combine($values);

        $this->assertEquals($expected, $combined);
        $this->assertEquals($keysCopy, $keys);
        $this->assertEquals($valuesCopy, $values);
    }

    public function testCount(): void
    {
        $arr = FluentArray::from([]);

        $this->assertEquals(0, $arr->count());

        $arr[] = '';
        $this->assertEquals(1, $arr->count());

        $arr[] = null;
        $this->assertEquals(2, $arr->count());

        unset($arr[0]);

        $this->assertEquals(1, $arr->count());
    }

    public function testCountValues(): void
    {
        $arr = FluentArray::from([1, 'hello', 1, 'world', 'hello']);
        $copy = clone $arr;
        $expected = FluentArray::from([
            1 => 2,
            'hello' => 2,
            'world' => 1,
        ]);

        $counted = $arr->countValues();

        $this->assertEquals($expected, $counted);
        $this->assertEquals($copy, $arr);
    }

    public function testCurrent(): void
    {
        $arr = FluentArray::from([0, 1, 2, 3]);

        $this->assertEquals(0, $arr->current());

        $arr->rewind();
        $this->assertEquals(0, $arr->current());

        $arr->next();
        $this->assertEquals(1, $arr->current());

        $arr->end();
        $this->assertEquals(3, $arr->current());

        $arr->next();
        $this->assertFalse($arr->current());
    }

    public function testDiff(): void
    {
        $arr1 = FluentArray::from(['a' => 'green', 'red', 'blue', 'red']);
        $arr2 = FluentArray::from(['b' => 'green', 'yellow', 'red']);
        $copy1 = clone $arr1;
        $copy2 = clone $arr2;

        $expected = FluentArray::from([1 => 'blue']);

        $diff = $arr1->diff($arr2);

        $this->assertEquals($expected, $diff);
        $this->assertEquals($copy1, $arr1);
        $this->assertEquals($copy2, $arr2);
    }

    public function testDiffAssoc(): void
    {
        $arr1 = FluentArray::from(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'red']);
        $arr2 = FluentArray::from(['a' => 'green', 'yellow', 'red']);
        $copy1 = clone $arr1;
        $copy2 = clone $arr2;

        $expected = FluentArray::from(['b' => 'brown', 'c' => 'blue', 0 => 'red']);

        $diff = $arr1->diffAssoc($arr2);

        $this->assertEquals($expected, $diff);
        $this->assertEquals($copy1, $arr1);
        $this->assertEquals($copy2, $arr2);
    }

    public function testDiffKey(): void
    {
        $arr1 = FluentArray::from(['blue' => 1, 'red' => 2, 'green' => 3, 'purple' => 4]);
        $arr2 = FluentArray::from(['green' => 5, 'yellow' => 7, 'cyan' => 8]);
        $copy1 = clone $arr1;
        $copy2 = clone $arr2;

        $expected = FluentArray::from(['blue' => 1, 'red' => 2, 'purple' => 4]);

        $diff = $arr1->diffKey($arr2);

        $this->assertEquals($expected, $diff);
        $this->assertEquals($copy1, $arr1);
        $this->assertEquals($copy2, $arr2);

    }

    public function testDiffUassoc(): void
    {
        $arr1 = FluentArray::from(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'red']);
        $arr2 = FluentArray::from(['a' => 'green', 'yellow', 'red']);
        $copy1 = clone $arr1;
        $copy2 = clone $arr2;

        $compare = function ($a, $b): int {
            return $a <=> $b;
        };

        $expected = FluentArray::from(['b' => 'brown', 'c' => 'blue', 0 => 'red']);

        $diff = $arr1->diffUassoc($arr2, $compare);

        $this->assertEquals($expected, $diff);
        $this->assertEquals($copy1, $arr1);
        $this->assertEquals($copy2, $arr2);
    }

    public function testDiffUkey(): void
    {
        $arr1 = FluentArray::from(['blue'  => 1, 'red'  => 2, 'green'  => 3, 'purple' => 4]);
        $arr2 = FluentArray::from(['green' => 5, 'blue' => 6, 'yellow' => 7, 'cyan'   => 8]);
        $copy1 = clone $arr1;
        $copy2 = clone $arr2;

        $compare = function ($key1, $key2): int {
            if ($key1 == $key2) {
                return 0;
            }

            return $key1 > $key2 ? 1 : -1;
        };

        $expected = FluentArray::from(['red' => 2, 'purple' => 4]);

        $diff = $arr1->diffUkey($arr2, $compare);

        $this->assertEquals($expected, $diff);
        $this->assertEquals($copy1, $arr1);
        $this->assertEquals($copy2, $arr2);
    }

    public function testEnd(): void
    {
        $arr = FluentArray::from([0, 1, 2]);


        $this->assertEquals(2, $arr->end());
        $this->assertEquals(2, $arr->current());

        $arr = FluentArray::from();

        $this->assertFalse($arr->end());
        $this->assertFalse($arr->current());
    }

    public function testFillKeys(): void
    {
        $keys = FluentArray::from(['foo', 5, 10, 'bar']);
        $copy = clone $keys;

        $expected = FluentArray::from(['foo' => 'banana', 5 => 'banana', 10 => 'banana', 'bar' => 'banana']);

        $filledKeys = $keys->fillKeys('banana');

        $this->assertEquals($expected, $filledKeys);
        $this->assertEquals($copy, $keys);
    }

    public function testFilter(): void
    {
        $arr = FluentArray::from([0, 'er', null, false, true, 5]);
        $copy = clone $arr;

        $expected = FluentArray::from([0, 5]);

        $filtered = $expected->filter('is_int');

        $this->assertEquals($expected, $filtered);
        $this->assertEquals($copy, $arr);

        $arr = FluentArray::from([0, 1, false, true, null, '', 'not']);
        $copy = clone $arr;

        $expected = FluentArray::from([1, true, 'not']);

        $filtered = $expected->filter();

        $this->assertEquals($expected, $filtered);
        $this->assertEquals($copy, $arr);
    }

    public function testFlip(): void
    {
        $arr = FluentArray::from(['oranges', 'apples', 'pears']);
        $copy = clone $arr;

        $expected = FluentArray::from(['oranges' => 0, 'apples' => 1, 'pears' => 2]);

        $flipped = $arr->flip();

        $this->assertEquals($expected, $flipped);
        $this->assertEquals($copy, $arr);
    }

    public function testInArray(): void
    {
        $arr = FluentArray::from([0, 1, 2, 3]);

        $this->assertTrue($arr->inArray(0));
        $this->assertTrue($arr->inArray(false));
        $this->assertFalse($arr->inArray(false, true));
    }

    public function testIntersect(): void
    {
        $arr1 = FluentArray::from(['a' => 'green', 'red', 'blue']);
        $arr2 = FluentArray::from(['b' => 'green', 'yellow', 'red']);
        $copy1 = clone $arr1;
        $copy2 = clone $arr2;

        $expected = FluentArray::from(['a' => 'green', 0 => 'red']);

        $intersected = $arr1->intersect($arr2);

        $this->assertEquals($expected, $intersected);
        $this->assertEquals($copy1, $arr1);
        $this->assertEquals($copy2, $arr2);
    }

    public function testIntersectAssoc(): void
    {
        $arr1 = FluentArray::from(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'red']);
        $arr2 = FluentArray::from(['a' => 'green', 'b' => 'yellow', 'blue', 'red']);
        $copy1 = clone $arr1;
        $copy2 = clone $arr2;

        $expected = FluentArray::from(['a' => 'green']);

        $intersected = $arr1->intersectAssoc($arr2);

        $this->assertEquals($expected, $intersected);
        $this->assertEquals($copy1, $arr1);
        $this->assertEquals($copy2, $arr2);
    }

    public function testIntersectKey(): void
    {
        $arr1 = FluentArray::from(['blue' => 1, 'red' => 2, 'green' => 3, 'purple' => 4]);
        $arr2 = FluentArray::from(['green' => 5, 'blue' => 6, 'yellow' => 7, 'cyan' => 8]);
        $copy1 = clone $arr1;
        $copy2 = clone $arr2;

        $expected = FluentArray::from(['blue' => 1, 'green' => 3]);

        $intersected = $arr1->intersectKey($arr2);

        $this->assertEquals($expected, $intersected);
        $this->assertEquals($copy1, $arr1);
        $this->assertEquals($copy2, $arr2);
    }

    public function testIntersectUassoc(): void
    {
        $arr1 = FluentArray::from(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'red']);
        $arr2 = FluentArray::from(['a' => 'GREEN', 'B' => 'brown', 'yellow', 'red']);
        $copy1 = clone $arr1;
        $copy2 = clone $arr2;

        $expected = FluentArray::from(['b' => 'brown']);

        $intersected = $arr1->intersectUassoc($arr2, 'strcasecmp');

        $this->assertEquals($expected, $intersected);
        $this->assertEquals($copy1, $arr1);
        $this->assertEquals($copy2, $arr2);
    }

    public function testIntersectUkey(): void
    {
        $arr1 = FluentArray::from(['blue'  => 1, 'red'  => 2, 'green'  => 3, 'purple' => 4]);
        $arr2 = FluentArray::from(['green' => 5, 'blue' => 6, 'yellow' => 7, 'cyan'   => 8]);
        $copy1 = clone $arr1;
        $copy2 = clone $arr2;

        $keyCompareFunction = function ($key1, $key2): int {
            if ($key1 == $key2) {
                return 0;
            }

            return $key1 > $key2 ? 1 : -1;
        };

        $expected = FluentArray::from(['blue' => 1, 'green' => 3]);

        $intersected = $arr1->intersectUkey($arr2, $keyCompareFunction);

        $this->assertEquals($expected, $intersected);
        $this->assertEquals($copy1, $arr1);
        $this->assertEquals($copy2, $arr2);
    }

    public function testImplode(): void
    {
        $arr = FluentArray::from(['lastname', 'email', 'phone']);
        $copy = clone $arr;

        $expected = 'lastname,email,phone';

        $imploded = $arr->implode(',');

        $this->assertEquals($expected, $imploded);
        $this->assertEquals($copy, $arr);
    }

    public function testKey(): void
    {
        $arr = FluentArray::from([0, 1, 2, 3, 4]);

        $this->assertSame(0, $arr->key());

        $arr->rewind();
        $this->assertSame(0, $arr->key());

        $arr->end();
        $this->assertSame(4, $arr->key());

        $arr->next();
        $this->assertNull($arr->key());
    }

    public function testKeyExists(): void
    {
        $arr = FluentArray::from([0 => 1, 'er' => false]);
        $copy = clone $arr;

        $this->assertFalse($arr->keyExists(-1));
        $this->assertEquals($copy, $arr);

        $this->assertTrue($arr->keyExists(0));
        $this->assertEquals($copy, $arr);

        $this->assertTrue($arr->keyExists('er'));
        $this->assertEquals($copy, $arr);
    }

    public function testKeyFirst(): void
    {
        $arr = FluentArray::from(['first' => 0, 'middle' => 1, 'last' => 2]);
        $copy = clone $arr;

        $this->assertEquals('first', $arr->keyFirst());
        $this->assertEquals($copy, $arr);
    }

    public function testKeyLast(): void
    {
        $arr = FluentArray::from(['first' => 0, 'middle' => 1, 'last' => 2]);
        $copy = clone $arr;

        $this->assertEquals('last', $arr->keyLast());
        $this->assertEquals($copy, $arr);
    }

    public function testKeys(): void
    {
        $arr = FluentArray::from([0 => 0, 'er' => false, 4 => 'another', 'A' => 0]);
        $copy = clone $arr;

        $expected = FluentArray::from([0, 'er', 4, 'A']);

        $keys = $arr->keys();

        $this->assertEquals($expected, $keys);
        $this->assertEquals($copy, $arr);
    }

    public function testKrsort(): void
    {
        $arr = FluentArray::from(['d' => 'lemon', 'a' => 'orange', 'b' => 'banana', 'c' => 'apple']);
        $copy = clone $arr;

        $expected = FluentArray::from(['d' => 'lemon', 'c' => 'apple', 'b' => 'banana', 'a' => 'orange']);

        $sorted = $arr->krsort();

        $this->assertEquals($expected, $sorted);
        $this->assertEquals($copy, $arr);
    }

    public function testKsort(): void
    {
        $arr = FluentArray::from(['d' => 'lemon', 'a' => 'orange', 'b' => 'banana', 'c' => 'apple']);
        $copy = clone $arr;

        $expected = FluentArray::from(['a' => 'orange', 'b' => 'banana', 'c' => 'apple', 'd' => 'lemon']);

        $sorted = $arr->krsort();

        $this->assertEquals($expected, $sorted);
        $this->assertEquals($copy, $arr);
    }

    public function testMap(): void
    {
        $arr1 = FluentArray::from([0, 1, 2, 3, 4, 5]);
        $copy1 = clone $arr1;

        $expected = FluentArray::from([0, 1, 8, 27, 64, 125]);

        $mapped = $arr1->map(function (int $n): int {
            return $n ** 3;
        });

        $this->assertEquals($expected, $mapped);
        $this->assertEquals($copy1, $arr1);

        $arr2 = FluentArray::from([0, 1, 2, 3, 4, 5]);
        $copy2 = clone $arr2;

        $expected = FluentArray::from([0, 1, 4, 9, 16, 25]);

        $mapped = $arr1->map(
            function (int $a, int $b): int {
                return $a * $b;
            },
            $arr2
        );

        $this->assertEquals($expected, $mapped);
        $this->assertEquals($copy1, $arr1);
        $this->assertEquals($copy2, $arr2);
    }

    public function testMax(): void
    {
        $this->assertEquals(5, FluentArray::from([0, 5, -8, 4, 5, 0])->max());
        $this->assertEquals('max', FluentArray::from(['aa', 'bb', 'max', 'cc', 'AA'])->max());
    }

    public function testMerge(): void
    {
        $arr1 = FluentArray::from(['color' => 'red', 2, 4]);
        $arr2 = FluentArray::from(['a', 'b', 'color' => 'green', 'shape' => 'trapezoid', 4]);
        $copy1 = clone $arr1;
        $copy2 = clone $arr2;

        $expected = FluentArray::from([
            'color' => 'green',
            0 => 2,
            1 => 4,
            2 => 'a',
            3 => 'b',
            'shape' => 'trapezoid',
            4 => 4,
        ]);

        $merged = $arr1->merge($arr2);

        $this->assertEquals($expected, $merged);
        $this->assertEquals($copy1, $arr1);
        $this->assertEquals($copy2, $arr2);
    }

    public function testMergeRecursive(): void
    {
        $arr1 = FluentArray::from(['color' => ['favorite' => 'red'], 5]);
        $arr2 = FluentArray::from([10, 'color' => ['favorite' => 'green', 'blue']]);
        $copy1 = clone $arr1;
        $copy2 = clone $arr2;

        $expected = FluentArray::from([
            'color' => [
                'favorite' => ['red', 'green'],
                0 => 'blue',
            ],
            0 => 5,
            1 => 10,
        ]);

        $merged = $arr1->mergeRecursive($arr2);

        $this->assertEquals($expected, $merged);
        $this->assertEquals($copy1, $arr1);
        $this->assertEquals($copy2, $arr2);
    }

    public function testMin(): void
    {
        $this->assertEquals(-8, FluentArray::from([0, 5, -8, 4, 5, 0])->min());
        $this->assertEquals('AA', FluentArray::from(['aa', 'bb', 'max', 'cc', 'AA'])->min());
    }

    public function testMultisort(): void
    {
        $arr = FluentArray::from(['10', 11, 100, 100, 'a']);
        $copy = clone $arr;

        $expected = FluentArray::from(['10', 100, 100, 11, 'a']);

        $sorted = $arr->multisort(SORT_ASC, SORT_STRING);

        $this->assertEquals($expected, $sorted);
        $this->assertEquals($copy, $arr);
    }

    public function testNatcasesort(): void
    {
        $arr = FluentArray::from(['IMG0.png', 'img12.png', 'img10.png', 'img2.png', 'img1.png', 'IMG3.png']);
        $copy = clone $arr;

        $expected = FluentArray::from([0 => 'IMG0.png', 4 => 'img1.png', 3 => 'img2.png', 5 => 'IMG3.png', 2 => 'img10.png', 1 => 'img12.png']);

        $sorted = $arr->natcasesort();

        $this->assertEquals($expected, $sorted);
        $this->assertEquals($copy, $arr);
    }

    public function testNatsort(): void
    {
        $arr = FluentArray::from(['img12.png', 'img10.png', 'img2.png', 'img1.png']);
        $copy = clone $arr;

        $expected = FluentArray::from([3 => 'img1.png', 2 => 'img2.png', 1 => 'img10.png', 0 => 'img12.png']);

        $sorted = $arr->natsort();

        $this->assertEquals($expected, $sorted);
        $this->assertEquals($copy, $arr);
    }

    public function testNext(): void
    {
        $arr = FluentArray::from([0, 1, 2, 3, 4]);

        $arr->rewind();

        $arr->next();
        $this->assertSame(1, $arr->current());

        $arr->next();
        $this->assertSame(2, $arr->current());

        $arr->end();
        $arr->next();
        $this->assertFalse($arr->valid());
    }

    public function testPad(): void
    {
        $arr = FluentArray::from([12, 10, 9]);
        $copy = clone $arr;

        $expected = FluentArray::from([12, 10, 9, 0, 0]);

        $padded = $arr->pad(5, 0);

        $this->assertEquals($expected, $padded);
        $this->assertEquals($copy, $arr);

        $expected = FluentArray::from([-1, -1, -1, -1, 12, 10, 9]);

        $padded = $arr->pad(-7, -1);

        $this->assertEquals($expected, $padded);
        $this->assertEquals($copy, $arr);

        $expected = FluentArray::from([12, 10, 9]);

        $padded = $arr->pad(2, 'noop');

        $this->assertEquals($expected, $padded);
        $this->assertEquals($copy, $arr);
    }

    public function testPop(): void
    {
        $arr = FluentArray::from(['orange', 'banana', 'apple', 'raspberry']);

        $expected = FluentArray::from(['orange', 'banana', 'apple']);

        $popped = $arr->pop();

        $this->assertEquals('raspberry', $popped);
        $this->assertEquals($expected, $arr);
    }

    public function testPrev(): void
    {
        $arr = FluentArray::from([0, 1, 2, 3, 4]);

        $this->assertFalse($arr->prev());

        $arr->reset();
        $this->assertFalse($arr->prev());

        $arr->end();
        $this->assertSame(3, $arr->prev());
    }

    public function testProduct(): void
    {
        $arr = FluentArray::from([2, 4, 6, 8]);
        $copy = clone $arr;

        $this->assertEquals(384, $arr->product());
        $this->assertEquals($copy, $arr);
    }

    public function testPush(): void
    {
        $arr = FluentArray::from(['orange', 'banana']);

        $expected = FluentArray::from(['orange', 'banana', 'apple', 'raspberry']);

        $pushed = $arr->push('apple', 'raspberry');

        $this->assertEquals($expected, $pushed);
    }

    public function testRand(): void
    {
        $arr = FluentArray::from(['Noe', 'Morpheus', 'Trinity', 'Cypher', 'Tank']);
        $copy = clone $arr;

        $rand = $arr->rand();

        $this->assertCount(1, $rand);
        $this->assertEquals($copy, $arr);

        $rand = $arr->rand(3);

        $this->assertCount(3, $rand);
        $this->assertEquals($copy, $arr);
    }

    public function testReduce(): void
    {
        $arr1 = FluentArray::from([1, 2, 3, 4, 5]);
        $copy1 = clone $arr1;

        $arr2 = FluentArray::from();
        $copy2 = clone $arr2;

        $sum = $arr1->reduce(function ($carry, $item) {
            return $carry + $item;
        });

        $this->assertEquals(15, $sum);
        $this->assertEquals($copy1, $arr1);

        $product = $arr1->reduce(
            function ($carry, $item) {
                return $carry * $item;
            },
            10
        );

        $this->assertEquals(1200, $product);
        $this->assertEquals($copy1, $arr1);

        $initial = $arr2->reduce(
            function ($carry, $item) {
                return $carry + $item;
            },
            'Initial value'
        );

        $this->assertEquals('Initial value', $initial);
        $this->assertEquals($copy2, $arr2);
    }

    public function testReplace(): void
    {
        $arr = FluentArray::from(['orange', 'banana', 'apple', 'raspberry']);
        $copy = clone $arr;
        $replacements1 = FluentArray::from([0 => 'pineapple', 4 => 'cherry']);
        $replacements2 = FluentArray::from([0 => 'grape']);

        $expected = FluentArray::from(['grape', 'banana', 'apple', 'raspberry', 'cherry']);

        $replaced = $copy->replace($replacements1, $replacements2);

        $this->assertEquals($expected, $replaced);
        $this->assertEquals($copy, $arr);
    }

    public function testReplaceRecursive(): void
    {
        $arr = FluentArray::from(['citrus' => ['orange'], 'berries' => ['blackberry', 'raspberry']]);
        $copy = clone $arr;

        $replacements = FluentArray::from(['citrus' => ['pineapple'], 'berries' => ['blueberry']]);

        $expected = FluentArray::from([
            'citrus' => ['pineapple'],
            'berries' => ['blueberry', 'raspberry'],
        ]);

        $replaced = $arr->replaceRecursive($replacements);

        $this->assertEquals($expected, $replaced);
        $this->assertEquals($copy, $arr);
    }

    public function testReset(): void
    {
        $arr1 = FluentArray::from([0, 1, 2, 3, 4]);
        $arr2 = FluentArray::from();

        $this->assertSame(0, $arr1->reset());

        $arr1->next();
        $this->assertSame(0, $arr1->reset());

        $this->assertFalse($arr2->reset());
    }

    public function testReverse(): void
    {
        $arr = FluentArray::from(['php', 4.0, ['green', 'red']]);
        $copy = clone $arr;

        $expected1 = FluentArray::from([['green', 'red'], 4, 'php']);
        $expected2 = FluentArray::from([2 => ['green', 'red'], 1 => 4, 0 => 'php']);

        $reversed = $arr->reverse();

        $this->assertEquals($expected1, $reversed);
        $this->assertEquals($copy, $arr);

        $reversed = $arr->reverse(true);

        $this->assertEquals($expected2, $reversed);
        $this->assertEquals($copy, $arr);
    }

    public function testRsort(): void
    {
        $arr = FluentArray::from(['lemon', 'orange', 'banana', 'apple']);
        $copy = clone $arr;

        $expected = FluentArray::from(['orange', 'lemon', 'banana', 'apple']);

        $sorted = $arr->rsort();

        $this->assertEquals($expected, $sorted);
        $this->assertEquals($copy, $arr);
    }

    public function testSearch(): void
    {
        $arr = FluentArray::from(['blue', 'red', 'green', 'red']);
        $copy = clone $arr;

        $this->assertEquals(2, $arr->search('green'));
        $this->assertEquals($copy, $arr);

        $this->assertEquals(1, $arr->search('red'));
        $this->assertEquals($copy, $arr);

        $this->assertFalse($arr->search('not found'));
        $this->assertEquals($copy, $arr);
    }

    public function testShift(): void
    {
        $arr = FluentArray::from(['orange', 'banana', 'apple', 'raspberry']);

        $expected = FluentArray::from(['banana', 'apple', 'raspberry']);

        $this->assertEquals('orange', $arr->shift());
        $this->assertEquals($expected, $arr);
    }

    public function testShuffle(): void
    {
        $arr = FluentArray::range(1, 20);
        $copy = clone $arr;

        $shuffled = $arr->shuffle();

        // it is unlikely to be equal, but it could
        $this->assertNotEquals($arr, $shuffled);
        $this->assertEquals($copy, $arr);
    }

    public function testSlice(): void
    {
        $arr = FluentArray::from(['a', 'b', 'c', 'd', 'e']);
        $copy = clone $arr;

        $expected1 = FluentArray::from(['c', 'd', 'e']);
        $expected2 = FluentArray::from(['d']);
        $expected3 = FluentArray::from(['a', 'b', 'c']);
        $expected4 = FluentArray::from(['c', 'd']);
        $expected5 = FluentArray::from([2 => 'c', 3 => 'd']);

        $sliced = $arr->slice(2);

        $this->assertEquals($expected1, $sliced);
        $this->assertEquals($copy, $arr);

        $sliced = $arr->slice(-2, 1);

        $this->assertEquals($expected2, $sliced);
        $this->assertEquals($copy, $arr);

        $sliced = $arr->slice(0, 3);

        $this->assertEquals($expected3, $sliced);
        $this->assertEquals($copy, $arr);

        $sliced = $arr->slice(2, -1);

        $this->assertEquals($expected4, $sliced);
        $this->assertEquals($copy, $arr);

        $sliced = $arr->slice(2, -1, true);

        $this->assertEquals($expected5, $sliced);
        $this->assertEquals($copy, $arr);
    }

    public function testSort(): void
    {
        $arr = FluentArray::from(['lemon', 'orange', 'banana', 'apple']);
        $copy = clone $arr;

        $expected = FluentArray::from(['apple', 'banana', 'lemon', 'orange']);

        $sorted = $arr->sort();

        $this->assertEquals($expected, $sorted);
        $this->assertEquals($copy, $arr);
    }

    public function testSplice(): void
    {
        $arr = FluentArray::from(['red', 'green', 'blue', 'yellow']);
        $copy = clone $arr;

        $expected1 = FluentArray::from(['red', 'green']);
        $expected2 = FluentArray::from(['red', 'yellow']);
        $expected3 = FluentArray::from(['red', 'orange']);
        $expected4 = FluentArray::from(['red', 'green', 'blue', 'black', 'maroon']);
        $expected5 = FluentArray::from(['red', 'green', 'blue', 'purple', 'yellow']);

        $spliced = $arr->splice(2);

        $this->assertEquals($expected1, $spliced);
        $this->assertEquals($copy, $arr);

        $spliced = $arr->splice(1, -1);

        $this->assertEquals($expected2, $spliced);
        $this->assertEquals($copy, $arr);

        $spliced = $arr->splice(1, count($arr), 'orange');

        $this->assertEquals($expected3, $spliced);
        $this->assertEquals($copy, $arr);

        $spliced = $arr->splice(-1, 1, FluentArray::from(['black', 'maroon']));

        $this->assertEquals($expected4, $spliced);
        $this->assertEquals($copy, $arr);

        $spliced = $arr->splice(3, 0, 'purple');

        $this->assertEquals($expected5, $spliced);
        $this->assertEquals($copy, $arr);
    }

    public function testSum(): void
    {
        $arr = FluentArray::from([2, 4, 6, 8]);
        $copy = clone $arr;

        $sum = $arr->sum();

        $this->assertEquals(20, $sum);
        $this->assertEquals($copy, $arr);
    }

    public function testUasort(): void
    {
        $arr = FluentArray::from([
            'a' => 4,
            'b' => 8,
            'c' => -1,
            'd' => -9,
            'e' => 2,
            'f' => 5,
            'g' => 3,
            'h' => -4,
        ]);
        $copy = clone $arr;

        $expected = FluentArray::from([
            'd' => -9,
            'h' => -4,
            'c' => -1,
            'e' => 2,
            'g' => 3,
            'a' => 4,
            'f' => 5,
            'b' => 8,
        ]);

        $sorted = $arr->uasort(function ($a, $b): int {
            return $a <=> $b;
        });

        $this->assertEquals($expected, $sorted);
        $this->assertEquals($copy, $arr);
    }

    public function testUdiff(): void
    {
        $class = new class(0, 0) {
            public $width;
            public $height;

            public function __construct(int $width, int $height)
            {
                $this->width = $width;
                $this->height = $height;
            }
        };

        $arr = FluentArray::from([
            new $class(11, 3),
            new $class(7, 1),
            new $class(2, 9),
            new $class(5, 7),
        ]);
        $copy = clone $arr;

        $expected = FluentArray::from([
            new $class(11, 3),
            new $class(7, 1),
        ]);

        $diff = $arr->udiff(
            FluentArray::from([
                new $class(7, 5),
                new $class(9, 2),
            ]),
            function ($a, $b): int {
                return $a->width * $a->height <=> $b->width * $b->height;
            }
        );

        $this->assertEquals($expected, $diff);
        $this->assertEquals($copy, $arr);
    }

    public function testUdiffAssoc(): void
    {
        $class = new class(0) {
            public $value;

            public function __construct($value)
            {
                $this->value = $value;
            }
        };

        $arr = FluentArray::from([
            '0.1' => new $class(9),
            '0.5' => new $class(12),
            0 => new $class(23),
            1 => new $class(4),
            2 => new $class(-15),
        ]);
        $copy = clone $arr;

        $expected = FluentArray::from([
            '0.1' => new $class(9),
            '0.5' => new $class(12),
            0 => new $class(23),
        ]);

        $diff = $arr->udiffAssoc(
            FluentArray::from([
                '0.2' => new $class(9),
                '0.5' => new $class(22),
                0 => new $class(3),
                1 => new $class(4),
                2 => new $class(-15),
            ]),
            function ($a, $b): int {
                return $a->value <=> $b->value;
            }
        );

        $this->assertEquals($expected, $diff);
        $this->assertEquals($copy, $arr);
    }

    public function testUdiffUassoc(): void
    {
        $class = new class(0) {
            public $value;

            public function __construct($value)
            {
                $this->value = $value;
            }
        };

        $arr = FluentArray::from([
            '0.1' => new $class(9),
            '0.5' => new $class(12),
            0 => new $class(23),
            1 => new $class(4),
            2 => new $class(-15),
        ]);
        $copy = clone $arr;

        $expected = FluentArray::from([
            '0.1' => new $class(9),
            '0.5' => new $class(12),
            0 => new $class(23),
        ]);

        $diff = $arr->udiffUassoc(
            FluentArray::from([
                '0.2' => new $class(9),
                '0.5' => new $class(22),
                0 => new $class(3),
                1 => new $class(4),
                2 => new $class(-15),
            ]),
            function ($a, $b): int {
                return $a->value <=> $b->value;
            },
            function ($a, $b): int {
                return $a <=> $b;
            }
        );

        $this->assertEquals($expected, $diff);
        $this->assertEquals($copy, $arr);
    }

    public function testUintersect(): void
    {
        $arr = FluentArray::from(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'red']);
        $copy = clone $arr;

        $expected = FluentArray::from([
            'a' => 'green',
            'b' => 'brown',
            0 => 'red',
        ]);

        $intersect = $arr->uintersect(
            FluentArray::from(['a' => 'GREEN', 'B' => 'brown', 'yellow', 'red']),
            'strcasecmp'
        );

        $this->assertEquals($expected, $intersect);
        $this->assertEquals($copy, $arr);
    }

    public function testUintersectAssoc(): void
    {
        $arr = FluentArray::from(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'red']);
        $copy = clone $arr;

        $expected = FluentArray::from([
            'a' => 'green',
        ]);

        $intersect = $arr->uintersectAssoc(
            FluentArray::from(['a' => 'GREEN', 'B' => 'brown', 'yellow', 'red']),
            'strcasecmp'
        );

        $this->assertEquals($expected, $intersect);
        $this->assertEquals($copy, $arr);
    }

    public function testUintersectUassoc(): void
    {
        $arr = FluentArray::from(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'red']);
        $copy = clone $arr;

        $expected = FluentArray::from([
            'a' => 'green',
            'b' => 'brown',
        ]);

        $intersect = $arr->uintersectUassoc(
            FluentArray::from(['a' => 'GREEN', 'B' => 'brown', 'yellow', 'red']),
            'strcasecmp',
            'strcasecmp'
        );

        $this->assertEquals($expected, $intersect);
        $this->assertEquals($copy, $arr);
    }

    public function testUksort(): void
    {
        $arr = FluentArray::from(['John' => 1, 'the Earth' => 2, 'an apple' => 3, 'a banana' => 4]);
        $copy = clone $arr;

        $expected = FluentArray::from(['an apple' => 3, 'a banana' => 4, 'the Earth' => 2, 'John' => 1]);

        $sorted = $arr->uksort(function ($a, $b) {
            $regex = '/^(a|an|the) /';

            return strcasecmp(
                preg_replace($regex, '', $a),
                preg_replace($regex, '', $b)
            );
        });

        $this->assertEquals($expected, $sorted);
        $this->assertEquals($copy, $arr);
    }

    public function testUnique(): void
    {
        $arr = FluentArray::from(['a' => 'green', 'red', 'b' => 'green', 'blue', 'red']);
        $copy = clone $arr;

        $expected = FluentArray::from(['a' => 'green', 0 => 'red', 1 => 'blue']);

        $unique = $arr->unique();

        $this->assertEquals($expected, $unique);
        $this->assertEquals($copy, $arr);
    }

    public function testUnshift(): void
    {
        $arr = FluentArray::from(['orange', 'banana']);

        $expected = FluentArray::from(['apple', 'raspberry', 'orange', 'banana']);

        $unshifted = $arr->unshift('apple', 'raspberry');

        $this->assertEquals($expected, $unshifted);
    }

    public function testUsort(): void
    {
        $arr = FluentArray::from([3, 2, 5, 6, 1]);
        $copy = clone $arr;

        $expected = FluentArray::from([1, 2, 3, 5, 6]);

        $sorted = $arr->usort(function (int $a, int $b): int {
            return $a <=> $b;
        });

        $this->assertEquals($expected, $sorted);
        $this->assertEquals($copy, $arr);
    }

    public function testToArray(): void
    {
        $storage = [
            'one',
            'more',
            'b' => 'example',
            0 => 'with',
            'nested' => [
                'array',
                'and' => [
                    'even',
                    'more',
                    5 => 'nested',
                ],
            ],
        ];

        $arr = FluentArray::from($storage);

        $this->assertEquals($storage, $arr->toArray());

        $recursive = [
            0,
            1,
            &$recursive,
        ];
        $arr = FluentArray::from($recursive);
        $arr->toArray();

//        $this->assertSame($recursive, $arr->toArray());

        $recursive = [
            0,
            1,
            [
                2,
                3,
                &$recursive,
            ],
        ];
        $arr = FluentArray::from($recursive);
        $arr->toArray();

//        $this->assertEquals($recursive, $arr->toArray());

        // FIXME how to test for recursive array? PHPUnit crashes over that test even if the values are right
    }

    public function testValues(): void
    {
        $arr = FluentArray::from(['size' => 'XL', 'color' => 'gold']);
        $copy = clone $arr;

        $expected = FluentArray::from(['XL', 'gold']);

        $values = $arr->values();

        $this->assertEquals($expected, $values);
        $this->assertEquals($copy, $arr);
    }

    public function testWalk(): void
    {
        $arr = FluentArray::from(['d' => 'lemon', 'a' => 'orange', 'b' => 'banana', 'c' => 'apple']);
        $copy = clone $arr;

        $expected = FluentArray::from([
            'd' => 'fruit: lemon',
            'a' => 'fruit: orange',
            'b' => 'fruit: banana',
            'c' => 'fruit: apple',
        ]);

        $walked = $arr->walk(
            function (&$item, $key, $uservalue): void {
                $item = sprintf('%s: %s', $uservalue, $item);
            },
            'fruit'
        );

        $this->assertEquals($expected, $walked);
        $this->assertEquals($copy, $arr);
    }

    public function testWalkRecursive(): void
    {
        $arr = FluentArray::from([
            'sweet' => ['a' => 'apple', 'b' => 'banana'],
            'sour' => 'lemon',
        ]);
        $copy = clone $arr;

        $expected = FluentArray::from([
            'sweet' => ['a' => 'elppa', 'b' => 'ananab'],
            'sour' => 'nomel',
        ]);

        $walked = $arr->walkRecursive(function (&$item): void {
            $item = strrev($item);
        });

        $this->assertEquals($expected, $walked);
        $this->assertEquals($copy, $arr);
    }
}
