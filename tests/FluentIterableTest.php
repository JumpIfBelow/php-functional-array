<?php

namespace Tests\JumpIfBelow\Arrays;

use JumpIfBelow\Arrays\Exception\{
    BadParameterException,
    NonScalarKeyException,
};
use JumpIfBelow\Arrays\FluentIterable;
use JumpIfBelow\Arrays\IterableOperator\{
    FilterOperator,
    MapOperator,
};
use PHPUnit\Framework\TestCase;

class FluentIterableTest extends TestCase
{
    public function testFrom(): void
    {
        $iterable = FluentIterable::from((function (): \Generator {
            for ($i = 5; $i < 18; $i += 3) {
                yield $i;
            }
        })());

        $this->assertEquals(
            [5, 8, 11, 14, 17],
            $iterable->toArray(),
        );

        $iterable = FluentIterable::from([0, -3, 'c', 5 => false]);

        $this->assertEquals(
            [0, -3, 'c', 5 => false],
            $iterable->toArray(),
        );
    }

    public function testRange(): void
    {
        $range = FluentIterable::range(-10, 10, 2.5);
        $this->assertEquals(
            [-10, -7.5, -5, -2.5, 0, 2.5, 5, 7.5, 10],
            $range->toArray(),
        );

        $range = FluentIterable::range(10, -10, 2.5);
        $this->assertEquals(
            [10, 7.5, 5, 2.5, 0, -2.5, -5, -7.5, -10],
            $range->toArray(),
        );

        $range = FluentIterable::range('a', 'k', 3);
        $this->assertEquals(
            ['a', 'd', 'g', 'j'],
            $range->toArray(),
        );

        $range = FluentIterable::range('k', 'a', 3);
        $this->assertEquals(
            ['k', 'h', 'e', 'b'],
            $range->toArray(),
        );

        $this->expectExceptionObject(new BadParameterException('The step must be an int if working with a string range.'));
        FluentIterable::range('a', 'j', 2.5);

        $this->expectExceptionObject(new BadParameterException('Start and end must either be both string or both numeric.'));
        FluentIterable::range(0, 'c');
    }

    public function testCount(): void
    {
        $this->assertEquals(10, FluentIterable::range(0, 9)->count());
        $this->assertEquals(5, FluentIterable::range(0, 9, 2)->count());
        $this->assertEquals(4, FluentIterable::from([false, null, 'string', 5])->count());
    }

    public function testCurrent(): void
    {
        $iterable = FluentIterable::from([
            false,
            null,
            'string',
            5,
            10,
        ]);

        $this->assertFalse($iterable->current());

        $iterable->next();
        $this->assertNull($iterable->current());

        $iterable->next();
        $this->assertEquals('string', $iterable->current());

        $iterable->next();
        $this->assertEquals(5, $iterable->current());

        $iterable->next();
        $this->assertEquals(10, $iterable->current());

        $iterable->next();
        $this->assertNull($iterable->current());

        $iterable->next();
        $this->assertNull($iterable->current());
    }

    public function testKey(): void
    {

        $iterable = FluentIterable::from([
            false,
            null,
            'string',
            'key' => 5,
            10,
        ]);

        $this->assertEquals(0, $iterable->key());

        $iterable->next();
        $this->assertEquals(1, $iterable->key());

        $iterable->next();
        $this->assertEquals(2, $iterable->key());

        $iterable->next();
        $this->assertEquals('key', $iterable->key());

        $iterable->next();
        $this->assertEquals(3, $iterable->key());

        $iterable->next();
        $this->assertNull($iterable->key());

        $iterable->next();
        $this->assertNull($iterable->key());
    }

    public function testNext(): void
    {
        $iterable = FluentIterable::from([
            false,
            null,
            'string',
            5,
            10,
        ]);

        $iterable->next();
        $this->assertEquals(1, $iterable->key());
        $this->assertNull($iterable->current());
        $this->assertTrue($iterable->valid());

        $iterable->next();
        $this->assertEquals(2, $iterable->key());
        $this->assertEquals('string', $iterable->current());
        $this->assertTrue($iterable->valid());

        $iterable->next();
        $iterable->next();
        $this->assertEquals(4, $iterable->key());
        $this->assertEquals(10, $iterable->current());
        $this->assertTrue($iterable->valid());

        $iterable->next();
        $this->assertNull($iterable->key());
        $this->assertNull($iterable->current());
        $this->assertFalse($iterable->valid());
    }

    public function testValid(): void
    {
        $iterable = FluentIterable::from();

        $this->assertFalse($iterable->valid());

        $iterable = FluentIterable::range(0, 4);

        $this->assertTrue($iterable->valid());

        $iterable->next();
        $this->assertTrue($iterable->valid());

        $iterable->next();
        $this->assertTrue($iterable->valid());

        $iterable->next();
        $this->assertTrue($iterable->valid());

        $iterable->next();
        $this->assertTrue($iterable->valid());

        $iterable->next();
        $this->assertFalse($iterable->valid());
    }

    public function testRewind(): void
    {
        $externalValue = null;
        $iterable = FluentIterable::from(function() use (&$externalValue) {
            $externalValue = true;
            yield 1;
        });

        $this->assertNull($externalValue);

        $iterable->rewind();
        $this->assertTrue($externalValue);
    }

    public function testApply(): void
    {
        $this->assertEquals(
            FluentIterable
                ::range(0, 9)
                ->map(fn (int $v): int => $v ** 2)
                ->toArray(),
            FluentIterable
                ::range(0, 9)
                ->apply(MapOperator::with(fn (int $v): int => $v ** 2))
                ->toArray(),
        );

        $this->assertEquals(
            FluentIterable
                ::range(0, 9)
                ->filter(fn (int $v): int => $v % 2 !== 0)
                ->toArray(),
            FluentIterable
                ::range(0, 9)
                ->apply(FilterOperator::with(fn (int $v): int => $v % 2 !== 0))
                ->toArray(),
        );

        $this->assertEquals(
            FluentIterable
                ::range(0, 9)
                ->map(fn (int $v): int => $v ** 2)
                ->filter(fn (int $v): int => $v % 2 !== 0)
                ->toArray(),
            FluentIterable
                ::range(0, 9)
                ->apply(
                    MapOperator::with(fn (int $v): int => $v ** 2),
                    FilterOperator::with(fn (int $v): int => $v % 2 !== 0),
                )
                ->toArray(),
        );

        $this->assertEquals(
            FluentIterable
                ::range(0, 9)
                ->apply(MapOperator::with(fn (int $v): int => $v ** 2))
                ->apply(FilterOperator::with(fn (int $v): int => $v % 2 !== 0))
                ->toArray(),
            FluentIterable
                ::range(0, 9)
                ->apply(
                    MapOperator::with(fn (int $v): int => $v ** 2),
                    FilterOperator::with(fn (int $v): int => $v % 2 !== 0),
                )
                ->toArray(),
        );


        $this->assertNotEquals(
            FluentIterable
                ::range(0, 9)
                ->apply(
                    FilterOperator::with(fn (int $v): int => $v % 2 !== 0),
                    MapOperator::with(fn (int $v): int => $v + 1),
                )
                ->toArray(),
            FluentIterable
                ::range(0, 9)
                ->apply(
                    MapOperator::with(fn (int $v): int => $v + 1),
                    FilterOperator::with(fn (int $v): int => $v % 2 !== 0),
                )
                ->toArray(),
        );
    }

    public function testForEach(): void
    {
        $iterable = FluentIterable::range(0, 4);
        $tester = function (int $value, int $key, iterable $self) use ($iterable): void {
            $this->assertEquals($key, $value);
            $this->assertSame($self, $iterable);
        };

        $iterable->forEach($tester);
    }

    public function testMap(): void
    {
        $iterable = FluentIterable::from([
            0,
            1,
            2,
            3,
        ]);

        $expected = FluentIterable::from([
            0,
            1,
            4,
            9,
        ]);

        $this->assertEquals(
            $expected->map(fn (int $value): int => $value),
            $iterable->map(fn (int $value): int => $value ** 2),
        );
    }

    public function testFilter(): void
    {
        $this->assertEquals(
            [
                0 => 0,
                2 => 2,
                4 => 4,
                6 => 6,
                8 => 8,
                10 => 10,
            ],
            FluentIterable
                ::range(0, 10)
                ->filter(fn (int $value): bool => $value % 2 === 0)
                ->toArray()
            ,
        );

        $this->assertEquals(
            [
                0 => 'long enough',
                3 => 'this one is too',
                4 => 'yet another one',
            ],
            FluentIterable
                ::from([
                    'long enough',
                    'not this',
                    'neither',
                    'this one is too',
                    'yet another one',
                    'but no',
                ])
                ->filter(fn (string $value): bool => strlen($value) > 8)
                ->toArray()
            ,
        );
    }

    public function testReduce(): void
    {
        $iterable = FluentIterable::range(0, 9);

        $this->assertEquals(
            45,
            $iterable->reduce(
                fn (int $previous, int $value): int => $previous + $value,
                0,
            ),
        );


        $iterable = FluentIterable::from(['e', 'x', 'a', 'm', 'p', 'l', 'e']);

        $this->assertEquals(
            'example',
            $iterable->reduce(
                fn (string $previous, string $value): string => $previous . $value,
                '',
            ),
        );
    }

    public function testReplace(): void
    {
        $iterable = FluentIterable::from([0, 1, 2, 3, 2, 4, 2]);

        $this->assertEquals(
            [0, 1, 5, 3, 5, 4, 5],
            $iterable
                ->replace(2, 5)
                ->toArray()
            ,
        );

        $iterable = FluentIterable::from([0, 1, 'a', 3, 4]);

        $this->assertEquals(
            [0, 1, 2, 3, 4],
            $iterable
                ->replace('a', 2)
                ->toArray()
            ,
        );

        $iterable = FluentIterable::from([0, '1', 2, '3', 4]);
        $this->assertEquals(
            [0, '1', 2, '3', 4],
            $iterable
                ->replace(1, 1)
                ->toArray()
            ,
        );

        $iterable = FluentIterable::from([0, '1', 2, '3', 4]);
        $this->assertEquals(
            [0, 1, 2, '3', 4],
            $iterable
                ->replace(1, 1, false)
                ->toArray()
            ,
        );
    }

    public function testSlice(): void
    {
        $iterable = FluentIterable::range(0, 9);

        $this->assertEquals(
            [0, 1, 2],
            $iterable
                ->slice(0, 3)
                ->toArray()
            ,
        );

        $iterable = FluentIterable::range(0, 9);

        $this->assertEquals(
            [
                5 => 5,
                6 => 6,
                7 => 7,
                8 => 8,
            ],
            $iterable
                ->slice(5, 4)
                ->toArray()
            ,
        );
    }

    public function testUnique(): void
    {
        $iterable = FluentIterable::from([0, 1, 2, 2, 3, 2, 4]);

        $this->assertEquals(
            [
                0 => 0,
                1 => 1,
                2 => 2,
                4 => 3,
                6 => 4,
            ],
            $iterable
                ->unique()
                ->toArray()
            ,
        );

        $iterable = FluentIterable::from([0, 1, '1', false, null, false, true]);

        $this->assertEquals(
            [
                0 => 0,
                1 => 1,
                2 => '1',
                3 => false,
                4 => null,
                6 => true,
            ],
            $iterable
                ->unique()
                ->toArray()
            ,
        );

        $iterable = FluentIterable::from([0, 1, '1', false, null, false, true]);

        $this->assertEquals(
            [0, 1],
            $iterable
                ->unique(false)
                ->toArray()
            ,
        );
    }

    public function testIndexBy(): void
    {
        $iterable = FluentIterable::range(0, 9);

        $this->assertEquals(
            [
                0 => 0,
                1 => 1,
                4 => 2,
                9 => 3,
                16 => 4,
                25 => 5,
                36 => 6,
                49 => 7,
                64 => 8,
                81 => 9,
            ],
            $iterable
                ->indexBy(fn (int $value): int => $value ** 2)
                ->toArray()
            ,
        );

        $iterable = FluentIterable::from([
            ['id' => 25, 'name' => 'John'],
            ['id' => 156, 'name' => 'George'],
            ['id' => 230, 'name' => 'Karen'],
        ]);

        $this->assertEquals(
            [
                25 => ['id' => 25, 'name' => 'John'],
                156 => ['id' => 156, 'name' => 'George'],
                230 => ['id' => 230, 'name' => 'Karen'],
            ],
            $iterable
                ->indexBy(fn (array $value): int => $value['id'])
                ->toArray()
            ,
        );
    }

    public function testSome(): void
    {
        $iterable = FluentIterable::range(0, 9);

        $this->assertFalse(
            $iterable->some(fn (int $value): bool => $value >= 10),
        );

        $iterable = FluentIterable::range(0, 9);

        $this->assertTrue(
            $iterable->some(fn (int $value): bool => $value % 2 === 0),
        );
    }

    public function testEvery(): void
    {
        $iterable = FluentIterable::range(0, 9);

        $this->assertFalse(
            $iterable->every(fn (int $value): bool => $value > 0),
        );

        $iterable = FluentIterable::range(0, 9);

        $this->assertTrue(
            $iterable->every(fn (int $value): bool => $value <= 9),
        );
    }

    public function testToArray(): void
    {
        $array = range(0, 9);

        $this->assertEquals(
            $array,
            FluentIterable
                ::from($array)
                ->toArray(),
        );


        $iterable = FluentIterable::from(function() {
            yield new \stdClass() => 5;
        });

        $this->expectExceptionObject(new NonScalarKeyException('A key was not a scalar, cannot convert to an array.'));
        $iterable->toArray();
    }
}
