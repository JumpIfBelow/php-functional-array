<?php

namespace JumpIfBelow\Arrays;

use JumpIfBelow\Arrays\Exception\{
    BadParameterException,
    ElementNotFoundException,
    NonScalarKeyException,
};
use JumpIfBelow\Arrays\IterableOperator\{
    FilterOperator,
    ForEachOperator,
    IndexByOperator,
    MapOperator,
    OperatorInterface,
    ReplaceOperator,
    SliceOperator,
    UniqueOperator,
};
use JumpIfBelow\Arrays\Traits\CallableParametersTrait;

/**
 * Works as a FluentArray, but deffer when the operators are called.
 * Instead of running the operators on all items at once, they are ran
 * as the pointer is advancing on each item.
 * This is intended for virtually infinite collections, where the
 * time to process would be too long and memory exhausted from any
 * called operator.
 */
class FluentIterable implements FluentIterableInterface {
    use CallableParametersTrait;

    /**
     * FluentIterable constructor.
     * @param \Generator $generator
     */
    protected function __construct(
        private \Generator $generator,
    ) {
        $this->generator = $generator;
    }

    /**
     * @inheritDoc
     */
    public static function from(iterable|callable $iterable = []): static
    {
        if (!is_callable($iterable)) {
            return static::from(function() use ($iterable): \Generator {
                foreach ($iterable as $key => $value) {
                    yield $key => $value;
                }
            });
        }

        return new static($iterable());
    }

    /**
     * Creates a {@see FluentIterableInterface} from a given range. It mimics the
     * way {@see \range}, but it has its own implementation.
     * @param string|int|float $start The beginning of the range. Could be a letter or a number.
     * @param string|int|float $end The end of the range. Could be a letter or a number.
     * @param int|float $step The size of each step. Must be an integer if using a letter.
     * @return static A new instance of {@see FluentIterableInterface}.
     * @see \range
     */
   public static function range(string|int|float $start, string|int|float $end, int|float $step = 1): static
    {
        if (is_numeric($start) !== is_numeric($end)) {
            throw new BadParameterException('Start and end must either be both string or both numeric.');
        }

        if (is_string($start) && !is_int($step)) {
            if (!is_int($step)) {
                throw new BadParameterException('The step must be an int if working with a string range.');
            }

            if (mb_strlen($start) !== 1 || mb_strlen($end) !== 1) {
                throw new BadParameterException('The range with string can only work with one character long.');
            }

        }

        if (is_string($start)) {
            if ($start <= $end) {
                return static::from(function() use ($start, $end, $step) {
                    for ($i = $start; $i <= $end; $i = mb_chr(mb_ord($i) + $step)) {
                        yield $i;
                    }
                });
            }

            return static::from(function() use ($start, $end, $step) {
                for ($i = $start; $i >= $end; $i = mb_chr(mb_ord($i) - $step)) {
                    yield $i;
                }
            });
        }

        if ($start <= $end) {
            return static::from(function() use ($start, $end, $step) {
                for ($i = $start; $i <= $end; $i += $step) {
                    yield $i;
                }
            });
        }

        return static::from(function() use ($start, $end, $step) {
            for ($i = $start; $i >= $end; $i -= $step) {
                yield $i;
            }
        });
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return iterator_count($this->generator);
    }

    /**
     * @inheritDoc
     */
    public function current(): mixed
    {
        if (!$this->valid()) {
            return null;
        }

        return $this->generator->current();
    }

    /**
     * @inheritDoc
     */
    public function key(): mixed
    {
        return $this->generator->key();
    }

    /**
     * @inheritDoc
     */
    public function next(): void
    {
        $this->generator->next();
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return $this->generator->valid();
    }

    /**
     * @inheritDoc
     */
    public function rewind(): void
    {
        $this->generator->rewind();
    }

    /**
     * @inheritDoc
     */
    public function apply(OperatorInterface $operatorInterface, OperatorInterface ...$operatorInterfaces): static
    {
        $fluentIterable = static::from(
            $operatorInterface->transform($this),
        );

        foreach ($operatorInterfaces as $operatorInterface) {
            $fluentIterable = static::from(
                $operatorInterface->transform($fluentIterable),
            );
        }

        return $fluentIterable;
    }

    /**
     * Iterates through the iterator.
     * @param callable $forEach The callable function to run on each iteration.
     * @return static The same iterator, allowing to do further actions.
     */
    public function forEach(callable $forEach): static
    {
        return $this->apply(ForEachOperator::with($forEach));
    }

    /**
     * Map the values into the iterable into another iterable.
     * @param callable $mapper The callable function returning the new value.
     * @return static A new iterator, with the values mapped by the function.
     */
    public function map(callable $mapper): static
    {
        return $this->apply(MapOperator::with($mapper));
    }

    /**
     * Filters out the values of the iterable.
     * @param callable $filter The callable function returning true or false
     * to keep or reject values.
     * @return static A new iterator, with the values unmatching the filter
     * opted out.
     */
    public function filter(callable $filter): static
    {
        return $this->apply(FilterOperator::with($filter));
    }

    /**
     * Reduce the whole iterable into a value made out of the
     * callable parameter.
     * @param callable $reducer The function receiving the values
     * and returning the accumalted value.
     * @param mixed $initial The initial value of the accumulator
     * to start with.
     */
    public function reduce(callable $reducer, mixed $initial = null): mixed
    {
        $accumulator = $initial;

        foreach ($this as $key => $value) {
            $accumulator = $reducer($accumulator, $value, $key, $this);
        }

        return $accumulator;
    }

    /**
     * Replaces each occurrence of a value by a new one.
     * @param mixed $search The searched value.
     * @param mixed $replace The value to replace with when matched.
     * @param bool $strict Either to strictly check equality (===) or not (==).
     * @return static A new iterable with all values matching the
     * search replaced by the replacement.
     */
    public function replace(mixed $search, mixed $replace, bool $strict = true): static
    {
        return $this->apply(ReplaceOperator::with($search, $replace, $strict));
    }

    /**
     * Slice the iterator into a smaller one.
     * @param int $offset The offset to start with. <b>Does not accept
     * negative values.</b>
     * @param int $length The number of values to keep. <b>Does not
     * accept negative values.</b>
     * @return static A new iterable sliced from the original one.
     */
    public function slice(int $offest, ?int $length = null): static
    {
        return $this->apply(SliceOperator::with($offest, $length));
    }

    /**
     * Filters out every duplicate values from the iterator.
     * @param bool $strict Either if uniqueness is tested strictly or
     * loosely. Works well with either small set or large ones with many
     * duplications. Should not be used on big set with only a few duplicates,
     * as it would exhaust the memory.
     * @return static A new iterable without any duplicate values.
     */
    public function unique(bool $strict = true): static
    {
        return $this->apply(UniqueOperator::with($strict));
    }

    /**
     * Alters the key of each entries with the provided callable.
     * @param callable $indexer The function that will return the new key.
     * Each key must be unique.
     * @return static A new iterable with the new keys.
     */
    public function indexBy(callable $indexer): static
    {
        return $this->apply(IndexByOperator::with($indexer));
    }

    /**
     * Validates that the test is validated by at least one element
     * in the whole set.
     * @param callable $callable The function that will return a boolean
     * for each value.
     * @return bool True if the callable returns true once. False otherwise.
     */
    public function some(callable $callable): bool
    {
        $clone = clone $this;
        $callable = static::slicedCallableParameters($callable);

        foreach ($this as $key => $value) {
            if ($callable($value, $key, $this)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Validates that the test is validated by the whole set.
     * @param callable The function that will return a boolean
     * for each value.
     * @return bool True if the callable returns true for all values.
     * False otherwise.
     */
    public function every(callable $callable): bool
    {
        $clone = clone $this;
        $callable = static::slicedCallableParameters($callable);

        foreach ($clone as $key => $value) {
            if (!$callable($value, $key, $clone)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Transforms the iterable into an array.
     * It may fail if the keys are non-scalar.
     * @throws NonScalarKeyException In case a non-scalar key is encountered.
     * In that case, it is impossible to use it in an associative array.
     */
    public function toArray(): array
    {
        $array = [];

        foreach ($this as $key => $value) {
            if (!is_scalar($key)) {
                throw new NonScalarKeyException('A key was not a scalar, cannot convert to an array.');
            }

            $array[$key] = $value;
        }

        return $array;
    }
}
