<?php

namespace JumpIfBelow\Arrays\Exception;

class NonScalarKeyException extends AbstractFunctionalArrayException
{
}
