<?php

namespace JumpIfBelow\Arrays\Exception;

class ZeroOrNegativeDepthException extends AbstractFunctionalArrayException
{
}
