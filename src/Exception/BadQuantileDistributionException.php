<?php

namespace JumpIfBelow\Arrays\Exception;

class BadQuantileDistributionException extends AbstractFunctionalArrayException
{
}
