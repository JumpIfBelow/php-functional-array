<?php

namespace JumpIfBelow\Arrays\Exception;

class BadOffsetException extends AbstractFunctionalArrayException
{
}
