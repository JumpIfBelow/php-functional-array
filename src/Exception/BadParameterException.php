<?php

namespace JumpIfBelow\Arrays\Exception;

class BadParameterException extends AbstractFunctionalArrayException
{
}
