<?php

namespace JumpIfBelow\Arrays\Exception;

class ElementNotFoundException extends AbstractFunctionalArrayException
{
}
