<?php

namespace JumpIfBelow\Arrays;

use JumpIfBelow\Arrays\Exception\BadOffsetException;
use JumpIfBelow\Arrays\Exception\ZeroOrNegativeDepthException;
use JumpIfBelow\Arrays\Traits\DepthTrait;

class FluentArray implements FluentArrayInterface
{
    use DepthTrait;

    /**
     * FluentArray constructor.
     * @param array $storage
     * @param int|null $depth
     */
    protected function __construct(
        protected array $storage = [],
        protected ?int $depth = null,
    ) {
    }

    /**
     * Deeply clones the array to make sure that any modification done in the array is not a reference to the same array.
     * Objects contained in the array are not handled: the goal is to make the structure immutable, not the values.
     */
    public function __clone(): void
    {
        foreach ($this as $key => $value) {
            if ($value instanceof FluentArrayInterface) {
                $this[$key] = clone $value;
            }
        }
    }

    /**
     * @inheritDoc
     */
    public static function from(iterable $iterable = [], ?int $depth = null): static
    {
        $convertedReferences = [];

        $recursiveFunction = function (iterable $iterable, ?int $depth) use (&$recursiveFunction, &$convertedReferences): static {
            $newArray = new static([], $depth);
            $canDeepen = static::canDeepen($depth);

            $convertedReferences[] = [$iterable, $newArray];

            if ($canDeepen) {
                $nextDepth = static::nextDepth($depth);
            }

            foreach ($iterable as $key => $value) {
                if ($canDeepen && is_iterable($value)) {
                    foreach ($convertedReferences as $convertedReference) {
                        if ($convertedReference[0] === $value) {
                            $newArray[$key] = $convertedReference[1];
                            continue 2;
                        }
                    }

                    $newArray[$key] = $recursiveFunction($value, $nextDepth);
                } else {
                    $newArray[$key] = $value;
                }
            }

            return $newArray;
        };

        return $recursiveFunction($iterable, $depth);
    }

    /**
     * @inheritDoc
     */
    public static function explode(string $delimiter, string $string, int $limit = PHP_INT_MAX): static
    {
        return static::from(explode(...func_get_args()));
    }

    /**
     * @inheritDoc
     */
    public static function pregSplit(string $pattern, string $subject, int $limit = -1, int $flags = 0): static
    {
        return static::from(preg_split(...func_get_args()));
    }

    /**
     * @inheritDoc
     */
    public static function fill(int $startIndex, int $num, mixed $value): static
    {
        return static::from(array_fill(...func_get_args()));
    }

    /**
     * @inheritDoc
     */
    public static function range(int|string $start, int|string $end, int|float $step = 1): static
    {
        return static::from(range(...func_get_args()));
    }

    /**
     * @param FluentArray $fluentArray
     * @param int|null $depth
     * @return array
     * @throws ZeroOrNegativeDepthException
     */
    protected static function fluentArrayToArray(FluentArray $fluentArray, ?int $depth = null): array
    {
        $processedArrays = [];

        $recursiveFunction = function (FluentArray $fluentArray, ?int $depth) use (&$recursiveFunction, &$processedArrays): array {
            $array = [];
            $depth ??= $fluentArray->depth;
            $canDeepen = static::canDeepen($depth);

            $processedArrays[] = [$fluentArray, &$array];

            if ($canDeepen) {
                $nextDepth = static::nextDepth($depth);
            }

            foreach ($fluentArray as $key => $value) {
                if ($canDeepen && $value instanceof FluentArray) {
                    foreach ($processedArrays as $processedArray) {
                        if ($processedArray[0] === $value) {
                            $array[$key] = &$processedArray[1];
                            continue 2;
                        }
                    }

                    $array[$key] = $recursiveFunction($value, $nextDepth);
                } else {
                    $array[$key] = $value;
                }
            }

            return $array;
        };

        return $recursiveFunction($fluentArray, $depth);
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return key($this->storage) !== null;
    }

    /**
     * @inheritDoc
     */
    public function rewind(): void
    {
        reset($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($offset): bool
    {
        return $this->keyExists($offset);
    }

    /**
     * @inheritDoc
     * @throws BadOffsetException
     */
    public function offsetGet(mixed $offset): mixed
    {
        if (!$this->offsetExists($offset)) {
            throw new BadOffsetException(sprintf(
                'The offset [%s] does not exists on this array.',
                is_int($offset) ? $offset : '"'.$offset.'"'
            ));
        }

        return $this->storage[$offset];
    }

    /**
     * @inheritDoc
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        if (is_array($value) && static::canDeepen($this->depth)) {
            $value = static::from($value, static::nextDepth($this->depth));
        }

        if ($offset === null) {
            $this->storage[] = $value;
        } else {
            $this->storage[$offset] = $value;
        }
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset(mixed $offset): void
    {
        unset($this->storage[$offset]);
    }

    /**
     * @inheritDoc
     */
    public function arsort(int $sortFlags = SORT_REGULAR): static
    {
        $array = clone $this;

        arsort($array->storage, ...func_get_args());

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function asort(int $sortFlags = SORT_REGULAR): static
    {
        $array = clone $this;

        asort($array->storage, ...func_get_args());

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function changeKeyCase(int $case = CASE_LOWER): static
    {
        return static::from(
            array_change_key_case($this->storage, ...func_get_args()),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function chunk(int $size, bool $preserveKeys = false): static
    {
        return static::from(
            array_chunk($this->storage, ...func_get_args()),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function column(mixed $columnKey, mixed $indexKey = null): static
    {
        return static::from(
            array_column($this->toArray(), ...func_get_args()),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function combine(FluentArrayInterface $values): static
    {
        return static::from(
            array_combine(...$this->getArrayParameters($values)),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function countValues(): static
    {
        return static::from(
            array_count_values($this->toArray()),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function current(): mixed
    {
        return current($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function diff(FluentArrayInterface $array, FluentArrayInterface ...$arrays): static
    {
        return static::from(
            array_diff(...$this->getArrayParameters($array, ...$arrays)),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function diffAssoc(FluentArrayInterface $array, FluentArrayInterface ...$arrays): static
    {
        return static::from(
            array_diff_assoc(...$this->getArrayParameters($array, ...$arrays)),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function diffKey(FluentArrayInterface $array, FluentArrayInterface ...$arrays): static
    {
        return static::from(
            array_diff_key(...$this->getArrayParameters($array, ...$arrays)),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function diffUassoc(FluentArrayInterface $array, callable $keyCompareFunc, FluentArrayInterface ...$arrays): static
    {
        $parameters = $this->getArrayParameters($array, ...$arrays);
        $parameters[] = $keyCompareFunc;

        return static::from(
            array_diff_uassoc(...$parameters),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function diffUkey(FluentArrayInterface $array, callable $keyCompareFunc, FluentArrayInterface ...$arrays): static
    {
        $parameters = $this->getArrayParameters($array, ...$arrays);
        $parameters[] = $keyCompareFunc;

        return static::from(
            array_diff_ukey(...$parameters),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function end(): mixed
    {
        return end($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function fillKeys($value): static
    {
        return static::from(
            array_fill_keys($this->storage, $value),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function filter(callable $callback = null, int $flag = 0): static
    {
        return static::from(
            array_filter($this->storage, ...func_get_args()),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function flip(): static
    {
        return static::from(
            array_flip($this->storage),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function inArray($needle, bool $strict = false): bool
    {
        return in_array($needle, $this->storage, $strict);
    }

    /**
     * @inheritDoc
     */
    public function intersect(FluentArrayInterface $array, FluentArrayInterface ...$arrays): static
    {
        return static::from(
            array_intersect(...$this->getArrayParameters($array, ...$arrays)),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function intersectAssoc(FluentArrayInterface $array, FluentArrayInterface ...$arrays): static
    {
        return static::from(
            array_intersect_assoc(...$this->getArrayParameters($array, ...$arrays)),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function intersectKey(FluentArrayInterface $array, FluentArrayInterface ...$arrays): static
    {
        return static::from(
            array_intersect_key(...$this->getArrayParameters($array, ...$arrays)),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function intersectUassoc(FluentArrayInterface $array, callable $keyCompareFunc, FluentArrayInterface ...$arrays): static
    {
        $parameters = $this->getArrayParameters($array, ...$arrays);
        $parameters[] = $keyCompareFunc;

        return static::from(
            array_intersect_uassoc(...$parameters),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function intersectUkey(FluentArrayInterface $array, callable $keyCompareFunc, FluentArrayInterface ...$arrays): static
    {
        $parameters = $this->getArrayParameters($array, ...$arrays);
        $parameters[] = $keyCompareFunc;

        return static::from(
            array_intersect_ukey(...$parameters),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function implode(string $glue = ''): string
    {
        return implode($glue, $this->storage);
    }

    /**
     * @inheritDoc
     */
    public function key(): int|string|null
    {
        return key($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function keyExists(int|string $key): bool
    {
        return array_key_exists($key, $this->storage);
    }

    /**
     * @inheritDoc
     */
    public function keyFirst(): int|string|null
    {
        return array_key_first($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function keyLast(): int|string|null
    {
        return array_key_last($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function keys($searchValue = null, bool $strict = false): static
    {
        return static::from(
            array_keys($this->storage, ...func_get_args()),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function krsort(int $sortFlags = SORT_REGULAR): static
    {
        $array = clone $this;

        krsort($array->storage, ...func_get_args());

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function ksort(int $sortFlags = SORT_REGULAR): static
    {
        $array = clone $this;

        ksort($array->storage, ...func_get_args());

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function map(?callable $callback, FluentArrayInterface ...$arrays): static
    {
        return static::from(
            array_map($callback, ...$this->getArrayParameters(...$arrays)),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function max(): mixed
    {
        return max($this->toArray());
    }

    /**
     * @inheritDoc
     */
    public function merge(FluentArrayInterface ...$arrays): static
    {
        return static::from(
            array_merge(...$this->getArrayParameters(...$arrays)),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function mergeRecursive(FluentArrayInterface ...$arrays): static
    {
        return static::from(
            array_merge_recursive(...$this->getArrayParameters(...$arrays)),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function min(): mixed
    {
        return min($this->toArray());
    }

    /**
     * @inheritDoc
     */
    public function multisort(int $sortOrder = SORT_ASC, int $sortFlags = SORT_REGULAR): static
    {
        $array = clone $this;

        array_multisort($array->storage, ...func_get_args());

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function natcasesort(): static
    {
        $array = clone $this;

        natcasesort($array->storage);

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function natsort(): static
    {
        $array = clone $this;

        natsort($array->storage);

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function next(): void
    {
        next($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function pad(int $size, mixed $value): static
    {
        return static::from(
            array_pad($this->storage, ...func_get_args()),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function pop(): mixed
    {
        return array_pop($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function prev(): mixed
    {
        return prev($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function product(): int|float
    {
        return array_product($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function push(...$values): static
    {
        $array = clone $this;

        array_push($array->storage, ...$values);

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function rand(int $num = 1): static
    {
        $rands = array_rand($this->storage, ...func_get_args());

        return is_array($rands)
            ? static::from($rands)
            : static::from([$rands])
        ;
    }

    /**
     * @inheritDoc
     */
    public function reduce(callable $callback, mixed $initial = null): mixed
    {
        return array_reduce($this->storage, ...func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function replace(FluentArrayInterface ...$arrays): static
    {
        return static::from(
            array_replace(...$this->getArrayParameters(...$arrays)),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function replaceRecursive(FluentArrayInterface ...$arrays): static
    {
        return static::from(
            array_replace_recursive(...$this->getArrayParameters(...$arrays)),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function reset(): mixed
    {
        return reset($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function reverse(bool $preserveKeys = false): static
    {
        return static::from(
            array_reverse($this->storage, ...func_get_args()),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function rsort(int $sortFlags = SORT_REGULAR): static
    {
        $array = clone $this;

        rsort($array->storage, ...func_get_args());

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function search(mixed $needle, bool $strict = false): int|string|false
    {
        return array_search($needle, $this->storage, $strict);
    }

    /**
     * @inheritDoc
     */
    public function shift(): mixed
    {
        return array_shift($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function shuffle(): static
    {
        $array = clone $this;

        shuffle($array->storage);

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function slice(int $offset, int $length = null, bool $preserveKeys = false): static
    {
        return static::from(
            array_slice($this->storage, ...func_get_args()),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function sort(int $sortFlags = SORT_REGULAR): static
    {
        $array = clone $this;

        sort($array->storage, ...func_get_args());

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function splice(int $offset, ?int $length = null, mixed $replacement = null): static
    {
        $array = clone $this;

        if (func_num_args() >= 3 && $replacement instanceof FluentArrayInterface) {
            $replacement = $replacement->toArray();
        }

        array_splice($array->storage, ...func_get_args());

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function sum(): int|float
    {
        return array_sum($this->storage);
    }

    /**
     * @inheritDoc
     */
    public function uasort(callable $valueCompareFunc): static
    {
        $array = clone $this;

        uasort($array->storage, ...func_get_args());

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function udiff(FluentArrayInterface $array, callable $valueCompareFunc, FluentArrayInterface ...$arrays): static
    {
        $parameters = $this->getArrayParameters($array, ...$arrays);
        $parameters[] = $valueCompareFunc;

        return static::from(
            array_udiff(...$parameters),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function udiffAssoc(FluentArrayInterface $array, callable $valueCompareFunc, FluentArrayInterface ...$arrays): static
    {
        $parameters = $this->getArrayParameters($array, ...$arrays);
        $parameters[] = $valueCompareFunc;

        return static::from(
            array_udiff_assoc(...$parameters),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function udiffUassoc(FluentArrayInterface $array, callable $valueCompareFunc, callable $keyCompareFunc, FluentArrayInterface ...$arrays): static
    {
        $parameters = $this->getArrayParameters($array, ...$arrays);
        $parameters[] = $valueCompareFunc;
        $parameters[] = $keyCompareFunc;

        return static::from(
            array_udiff_uassoc(...$parameters),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function uintersect(FluentArrayInterface $array, callable $valueCompareFunc, FluentArrayInterface ...$arrays): static
    {
        $parameters = $this->getArrayParameters($array, ...$arrays);
        $parameters[] = $valueCompareFunc;

        return static::from(
            array_uintersect(...$parameters),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function uintersectAssoc(FluentArrayInterface $array, callable $valueCompareFunc, FluentArrayInterface ...$arrays): static
    {
        $parameters = $this->getArrayParameters($array, ...$arrays);
        $parameters[] = $valueCompareFunc;

        return static::from(
            array_uintersect_assoc(...$parameters),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function uintersectUassoc(FluentArrayInterface $array, callable $valueCompareFunc, callable $keyCompareFunc, FluentArrayInterface ...$arrays): static
    {
        $parameters = $this->getArrayParameters($array, ...$arrays);
        $parameters[] = $valueCompareFunc;
        $parameters[] = $keyCompareFunc;

        return static::from(
            array_uintersect_uassoc(...$parameters),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function uksort(callable $keyCompareFunc): static
    {
        $array = clone $this;

        uksort($array->storage, ...func_get_args());

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function unique(int $sortFlags = SORT_STRING): static
    {
        return static::from(
            array_unique($this->storage, ...func_get_args()),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function unshift(...$values): static
    {
        $array = clone $this;

        array_unshift($array->storage, ...$values);

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function usort(callable $valueCompareFunc): static
    {
        $array = clone $this;

        usort($array->storage, ...func_get_args());

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return static::fluentArrayToArray($this, $this->depth);
    }

    /**
     * @inheritDoc
     */
    public function values(): static
    {
        return static::from(
            array_values($this->storage),
            $this->depth
        );
    }

    /**
     * @inheritDoc
     */
    public function walk(callable $callback, mixed $userData = null): static
    {
        $array = clone $this;

        array_walk($array->storage, ...func_get_args());

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function walkRecursive(callable $callback, mixed $userData = null): static
    {
        $array = $this->toArray();

        array_walk_recursive($array, ...func_get_args());

        return static::from(
            $array,
            $this->depth
        );
    }

    /**
     * @param FluentArrayInterface ...$arrays
     * @return array
     * @throws ZeroOrNegativeDepthException
     */
    protected function getArrayParameters(FluentArrayInterface ...$arrays): array
    {
        array_unshift($arrays, $this);

        return static::fluentArrayToArray(static::from($arrays), $this->depth);
    }
}
