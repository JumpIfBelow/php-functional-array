<?php

namespace JumpIfBelow\Arrays;

use JumpIfBelow\Arrays\IterableOperator\OperatorInterface;

interface FluentIterableInterface extends \Countable, \Iterator
{
    /**
     * Creates an instance from a given iterable.
     * @param iterable $iterable The iterable to create the object from, or a
     * callable without any parameter returning an iterable.
     * @return static A new instance of {@see FluentIterableInterface}.
     */
    public static function from(iterable|callable $iterable = []): static;

    /**
     * @return int
     * @inheritDoc
     * @see \Countable::count()
     * @see count()
     */
    public function count(): int;

    /**
     * @return mixed|false
     * @inheritDoc
     * @see \Iterator::current()
     * @see current()
     */
    public function current(): mixed;

    /**
     * @return mixed
     * @inheritDoc
     * @see \Iterator::key()
     * @see key()
     */
    public function key(): mixed;

    /**
     * @return mixed|false
     * @inheritDoc
     * @see \Iterator::next()
     * @see next()
     */
    public function next(): void;

    /**
     * @return void
     * @inheritDoc
     * @see \Iterator::rewind()
     */
    public function rewind(): void;

    /**
     * @return bool
     * @inheritDoc
     * @see \Iterator::valid()
     */
    public function valid(): bool;

    /**
     * Apply one to n {@see OperatorInterface} to the current iterable.
     * @param OperatorInterface $operatorInterface The mandatory operator.
     * @param OperatorInterface ...$operatorInterfaces Following operators.
     * @return static A new {@see FluentIterableInterface} that will have
     * the operators applied while iterating through it.
     */
    public function apply(OperatorInterface $operatorInterface, OperatorInterface ...$operatorInterfaces): static;
}
