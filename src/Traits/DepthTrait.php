<?php

namespace JumpIfBelow\Arrays\Traits;

trait DepthTrait {
    /**
     * @param int|null $depth
     * @return int|null
     * @throws ZeroOrNegativeDepthException
     */
    protected static function nextDepth(?int $depth): ?int
    {
        if (!static::canDeepen($depth)) {
            throw new ZeroOrNegativeDepthException();
        }

        return $depth === null
            ? null
            : $depth - 1
        ;
    }

    protected static function canDeepen(?int $depth): bool
    {
        return $depth === null || $depth > 0;
    }
}
