<?php

namespace JumpIfBelow\Arrays\Traits;

trait CallableParametersTrait {
    /**
     * Generate a function that slices the number of parameters to fit the function signature.
     * Useful mainly with native PHP function as they do not allow to use more parameters than intended.
     * @param callable $callable
     * @return callable The same function as the parameter, but dropping extra parameters.
     * @throws \ReflectionException
     */
    protected static function slicedCallableParameters(callable $callable): callable
    {
        $reflectionFunction = new \ReflectionFunction($callable);
        $numberOfParameters = $reflectionFunction->getNumberOfParameters();

        $slicer =  fn(...$parameters): array => array_slice($parameters, 0, $numberOfParameters);

        return fn (...$args) => $callable(...$slicer(...$args));
    }
}
