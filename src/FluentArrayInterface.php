<?php

namespace JumpIfBelow\Arrays;

/**
 * Interface FluentArrayInterface describes all of the built-in PHP array functions.
 * Each call that returns the interface itself must be immutable.
 * @package JumpIfBelow\Arrays
 * @link https://secure.php.net/manual/en/ref.array.php
 */
interface FluentArrayInterface extends \ArrayAccess, \Countable, \Iterator
{
    /**
     * @param iterable $iterable
     * @param int|null $depth
     * @return static
     */
    public static function from(iterable $iterable = [], ?int $depth = null): static;

    /**
     * @return int
     * @inheritDoc
     * @see \Countable::count()
     * @see count()
     */
    public function count(): int;

    /**
     * @return mixed|false
     * @inheritDoc
     * @see \Iterator::current()
     * @see current()
     */
    public function current(): mixed;

    /**
     * @return mixed|false
     * @inheritDoc
     * @see \Iterator::next()
     * @see next()
     */
    public function next(): void;

    /**
     * @return int|string|null
     * @inheritDoc
     * @see \Iterator::key()
     * @see key()
     */
    public function key(): int|string|null;

    /**
     * @return void
     * @inheritDoc
     * @see \Iterator::rewind()
     */
    public function rewind(): void;

    /**
     * @return bool
     * @inheritDoc
     * @see \Iterator::valid()
     */
    public function valid(): bool;

    /**
     * @param int|string $offset
     * @return bool
     * @inheritDoc
     * @see \ArrayAccess::offsetExists()
     */
    public function offsetExists(mixed $offset): bool;

    /**
     * @param int|string $offset
     * @return mixed
     * @inheritDoc
     * @see \ArrayAccess::offsetGet()
     */
    public function offsetGet(mixed $offset): mixed;

    /**
     * @param int|string|null $offset
     * @param mixed $value
     * @return void
     * @inheritDoc
     * @see \ArrayAccess::offsetSet()
     */
    public function offsetSet(mixed $offset, mixed $value): void;

    /**
     * @param int|string $offset
     * @return void
     * @inheritDoc
     * @see \ArrayAccess::offsetUnset()
     */
    public function offsetUnset(mixed $offset): void;

    /**
     * @param string $delimiter
     * @param string $string
     * @param int $limit
     * @return static
     * @see explode()
     */
    public static function explode(string $delimiter, string $string, int $limit = PHP_INT_MAX): static;

    /**
     * @param string $pattern
     * @param string $subject
     * @param int $limit
     * @param int $flags
     * @return static
     * @see preg_split()
     */
    public static function pregSplit(string $pattern, string $subject, int $limit = -1, int $flags = 0): static;

    /**
     * @param int $startIndex
     * @param int $num
     * @param mixed $value
     * @return static
     * @see array_fill()
     */
    public static function fill(int $startIndex, int $num, mixed $value): static;

    /**
     * @param int|string $start
     * @param int|string $end
     * @param int|float $step
     * @return static
     * @see range()
     */
    public static function range(int|string $start, int|string $end, int|float $step  = 1): static;

    /**
     * @param int $sortFlags
     * @return static
     * @see arsort()
     */
    public function arsort(int $sortFlags = SORT_REGULAR): static;

    /**
     * @param int $sortFlags
     * @return static
     * @see asort()
     */
    public function asort(int $sortFlags = SORT_REGULAR): static;

    /**
     * @param int $case
     * @return static
     * @see array_change_key_case()
     */
    public function changeKeyCase(int $case = CASE_LOWER): static;

    /**
     * @param int $size
     * @param  bool $preserveKeys
     * @return static
     * @see array_chunk()
     */
    public function chunk(int $size, bool $preserveKeys = false): static;

    /**
     * @param int|string|null $columnKey
     * @param int|string|null $indexKey
     * @return static
     * @see array_column()
     */
    public function column(int|string|null $columnKey, int|string|null $indexKey = null): static;

    /**
     * @param self $values
     * @return static
     * @see array_combine()
     */
    public function combine(self $values): static;

    /**
     * @return static
     * @see array_count_values()
     */
    public function countValues(): static;

    /**
     * @param self $array
     * @param self ...$arrays
     * @return static
     * @see array_diff()
     */
    public function diff(self $array, self ...$arrays): static;

    /**
     * @param self $array
     * @param self ...$arrays
     * @return static
     * @see array_diff_assoc()
     */
    public function diffAssoc(self $array, self ...$arrays): static;

    /**
     * @param self $array
     * @param self ...$arrays
     * @return static
     * @see array_diff_key()
     */
    public function diffKey(self $array, self ...$arrays): static;

    /**
     * @param self $array
     * @param callable $keyCompareFunc
     * @param self ...$arrays
     * @return static
     * @see array_diff_uassoc()
     */
    public function diffUassoc(self $array, callable $keyCompareFunc, self ...$arrays): static;

    /**
     * @param self $array
     * @param callable $keyCompareFunc
     * @param self ...$arrays
     * @return static
     * @see array_diff_key()
     */
    public function diffUkey(self $array, callable $keyCompareFunc, self ...$arrays): static;

    /**
     * @return mixed
     * @see end()
     */
    public function end(): mixed;

    /**
     * @param mixed $value
     * @return static
     * @see array_fill_keys()
     */
    public function fillKeys(mixed $value): static;

    /**
     * @param callable|null $callback
     * @param int $flag
     * @return static
     * @see array_filter()
     */
    public function filter(?callable $callback = null, int $flag = 0): static;

    /**
     * @return static
     * @see array_flip()
     */
    public function flip(): static;

    /**
     * @param mixed $needle
     * @param bool $strict
     * @return bool
     * @see in_array()
     */
    public function inArray(mixed $needle, bool $strict = false): bool;

    /**
     * @param self $array
     * @param self ...$arrays
     * @return static
     * @see array_intersect()
     */
    public function intersect(self $array, self ...$arrays): static;

    /**
     * @param self $array
     * @param self ...$arrays
     * @return static
     * @see array_intersect_assoc()
     */
    public function intersectAssoc(self $array, self ...$arrays): static;

    /**
     * @param self $array
     * @param self ...$arrays
     * @return static
     * @see array_intersect_key()
     */
    public function intersectKey(self $array, self ...$arrays): static;

    /**
     * @param self $array
     * @param callable $keyCompareFunc
     * @param self ...$arrays
     * @return static
     * @see array_intersect_uassoc()
     */
    public function intersectUassoc(self $array, callable $keyCompareFunc, self ...$arrays): static;

    /**
     * @param self $array
     * @param callable $keyCompareFunc
     * @param self ...$arrays
     * @return static
     * @see array_intersect_ukey()
     */
    public function intersectUkey(self $array, callable $keyCompareFunc, self ...$arrays): static;

    /**
     * @param string $glue
     * @return string
     * @see implode()
     */
    public function implode(string $glue = ''): string;

    /**
     * @param int|string $key
     * @return bool
     * @see array_key_exists()
     */
    public function keyExists(int|string $key): bool;

    /**
     * @return int|string|null
     * @see array_key_first()
     */
    public function keyFirst(): int|string|null;

    /**
     * @return int|string|null
     * @see array_key_last()
     */
    public function keyLast(): int|string|null;

    /**
     * @param mixed $searchValue
     * @param bool $strict
     * @return static
     * @see array_keys()
     */
    public function keys(mixed $searchValue = null, bool $strict = false): static;

    /**
     * @param int $sortFlags
     * @return static
     * @see krsort()
     */
    public function krsort(int $sortFlags = SORT_REGULAR): static;

    /**
     * @param int $sortFlags
     * @return static
     * @see ksort()
     */
    public function ksort(int $sortFlags = SORT_REGULAR): static;

    /**
     * @param callable|null $callback
     * @param self ...$arrays
     * @return static
     * @see array_map()
     */
    public function map(?callable $callback, self ...$arrays): static;

    /**
     * @return mixed
     * @see max()
     */
    public function max(): mixed;

    /**
     * @param self ...$arrays
     * @return static
     * @see array_merge()
     */
    public function merge(self ...$arrays): static;

    /**
     * @param self ...$arrays
     * @return static
     * @see array_merge_recursive()
     */
    public function mergeRecursive(self ...$arrays): static;

    /**
     * @return mixed
     * @see min()
     */
    public function min(): mixed;

    /**
     * @param int $sortOrder
     * @param int $sortFlags
     * @return static
     * @see array_multisort()
     */
    public function multisort(int $sortOrder = SORT_ASC, int $sortFlags = SORT_REGULAR): static;

    /**
     * @return static
     * @see natcasesort()
     */
    public function natcasesort(): static;

    /**
     * @return static
     * @see natsort()
     */
    public function natsort(): static;

    /**
     * @param int $size
     * @param mixed $value
     * @return static
     * @see array_pad()
     */
    public function pad(int $size, mixed $value): static;

    /**
     * @return mixed
     * @see array_pop()
     */
    public function pop(): mixed;

    /**
     * @return mixed
     * @see prev()
     */
    public function prev(): mixed;

    /**
     * @return int|float
     * @see array_product()
     */
    public function product(): int|float;

    /**
     * @param mixed ...$values
     * @return static
     * @see array_push()
     */
    public function push(mixed ...$values): static;

    /**
     * @param int $num
     * @return static
     * @see array_rand()
     */
    public function rand(int $num = 1): static;

    /**
     * @param callable $callback
     * @param mixed $initial
     * @return mixed
     * @see array_reduce()
     */
    public function reduce(callable $callback, mixed $initial = null): mixed;

    /**
     * @param self ...$arrays
     * @return static
     * @see array_replace()
     */
    public function replace(self ...$arrays): static;

    /**
     * @param self ...$arrays
     * @return static
     * @see array_replace_recursive()
     */
    public function replaceRecursive(self ...$arrays): static;

    /**
     * @return mixed
     * @see reset()
     */
    public function reset(): mixed;

    /**
     * @param bool $preserveKeys
     * @return static
     * @see array_reverse()
     */
    public function reverse(bool $preserveKeys = false): static;

    /**
     * @param int $sortFlags
     * @return static
     * @see rsort()
     */
    public function rsort(int $sortFlags = SORT_REGULAR): static;

    /**
     * @param mixed $needle
     * @param bool $strict
     * @return int|string|false
     * @see array_search()
     */
    public function search(mixed $needle, bool $strict = false): int|string|false;

    /**
     * @return mixed
     * @see array_shift()
     */
    public function shift(): mixed;

    /**
     * @return static
     * @see shuffle()
     */
    public function shuffle(): static;

    /**
     * @param int $offset
     * @param int|null $length
     * @param bool $preserveKeys
     * @return static
     * @see array_slice()
     */
    public function slice(int $offset, ?int $length = null, bool $preserveKeys = false): static;

    /**
     * @param int $sortFlags
     * @return static
     * @see sort()
     */
    public function sort(int $sortFlags = SORT_REGULAR): static;

    /**
     * @param int $offset
     * @param int|null $length
     * @param self|mixed $replacement
     * @return static
     * @see array_splice()
     */
    public function splice(int $offset, ?int $length = null, mixed $replacement = null): static;

    /**
     * @return int|float
     * @see array_sum()
     */
    public function sum(): int|float;

    /**
     * @param callable $valueCompareFunc
     * @return static
     * @see uasort()
     */
    public function uasort(callable $valueCompareFunc): static;

    /**
     * @param self $array
     * @param callable $valueCompareFunc
     * @param self ...$arrays
     * @return static
     * @see array_udiff()
     */
    public function udiff(self $array, callable $valueCompareFunc, self ...$arrays): static;

    /**
     * @param self $array
     * @param callable $valueCompareFunc
     * @param self ...$arrays
     * @return static
     * @see array_udiff_assoc()
     */
    public function udiffAssoc(self $array, callable $valueCompareFunc, self ...$arrays): static;

    /**
     * @param self $array
     * @param callable $valueCompareFunc
     * @param callable $keyCompareFunc
     * @param self ...$arrays
     * @return static
     * @see array_udiff_uassoc()
     */
    public function udiffUassoc(self $array, callable $valueCompareFunc, callable $keyCompareFunc, self ...$arrays): static;

    /**
     * @param self $array
     * @param callable $valueCompareFunc
     * @param self ...$arrays
     * @return static
     * @see array_uintersect()
     */
    public function uintersect(self $array, callable $valueCompareFunc, self ...$arrays): static;

    /**
     * @param self $array
     * @param callable $valueCompareFunc
     * @param self ...$arrays
     * @return static
     * @see array_uintersect_assoc()
     */
    public function uintersectAssoc(self $array, callable $valueCompareFunc, self ...$arrays): static;

    /**
     * @param self $array
     * @param callable $valueCompareFunc
     * @param callable $keyCompareFunc
     * @param self ...$arrays
     * @return static
     * @see array_uintersect_uassoc()
     */
    public function uintersectUassoc(self $array, callable $valueCompareFunc, callable $keyCompareFunc, self ...$arrays): static;

    /**
     * @param callable $keyCompareFunc
     * @return static
     * @see uksort()
     */
    public function uksort(callable $keyCompareFunc): static;

    /**
     * @param int $sortFlags
     * @return static
     * @see array_unique()
     */
    public function unique(int $sortFlags = SORT_STRING): static;

    /**
     * @param mixed ...$values
     * @return static
     * @see array_unshift()
     */
    public function unshift(...$values): static;

    /**
     * @param callable $valueCompareFunc
     * @return static
     * @see usort()
     */
    public function usort(callable $valueCompareFunc): static;

    /**
     * Converts the actual object into a native array.
     * @return array
     */
    public function toArray(): array;

    /**
     * @return static
     * @see array_values()
     */
    public function values(): static;

    /**
     * @param callable $callback
     * @param mixed $userData
     * @return static
     * @see array_walk()
     */
    public function walk(callable $callback, mixed $userData = null): static;

    /**
     * @param callable $callback
     * @param mixed $userData
     * @return static
     * @see array_walk_recursive()
     */
    public function walkRecursive(callable $callback, mixed $userData = null): static;
}
