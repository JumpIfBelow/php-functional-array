<?php

namespace JumpIfBelow\Arrays;

use JumpIfBelow\Arrays\Exception\BadOffsetException;
use JumpIfBelow\Arrays\Exception\BadQuantileDistributionException;
use JumpIfBelow\Arrays\Exception\ElementNotFoundException;
use JumpIfBelow\Arrays\Exception\ZeroOrNegativeDepthException;
use JumpIfBelow\Arrays\Traits\CallableParametersTrait;

class ExtendedArray extends FluentArray implements ExtendedArrayInterface
{
    use CallableParametersTrait;

    /**
     * @param float $distribution
     * @throws BadQuantileDistributionException
     */
    protected function validateDistribution(float $distribution): void
    {
        if ($distribution <= 0 || $distribution > 0.5 || ((int) ($distribution ** -1)) != ($distribution ** -1)) {
            throw new BadQuantileDistributionException();
        }
    }

    /**
     * @inheritDoc
     */
    public function forEach(callable $callable): static
    {
        $clone = clone $this;
        $callable = static::slicedCallableParameters($callable);

        foreach ($clone as $key => $value) {
            $callable($value, $key, $clone);
        }

        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function every(callable $callable): bool
    {
        $clone = clone $this;
        $callable = static::slicedCallableParameters($callable);

        foreach ($this as $key => $value) {
            if (!$callable($value, $key, $clone)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function some(callable $callable): bool
    {
        $clone = clone $this;
        $callable = static::slicedCallableParameters($callable);

        foreach ($this as $key => $value) {
            if ($callable($value, $value, $clone)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function entry(): ?Entry
    {
        $key = $this->key();

        if ($key === null) {
            return null;
        }

        $value = $this->storage[$key];

        return new Entry($key, $value);
    }

    /**
     * @inheritDoc
     */
    public function nextKey(): int|string|null
    {
        $this->next();

        return $this->key();
    }


    /**
     * @inheritDoc
     */
    public function nextValue(): mixed
    {
        $nextKey = $this->nextKey();

        if ($nextKey === null) {
            return null;
        }

        return $this->storage[$nextKey];
    }


    /**
     * @inheritDoc
     */
    public function nextEntry(): ?Entry
    {
        $nextKey = $this->nextKey();

        if ($nextKey === null) {
            return null;
        }

        return $this->entry();
    }

    /**
     * @inheritDoc
     */
    public function findKey(callable $callable): int|string
    {
        $clone = clone $this;
        $callable = static::slicedCallableParameters($callable);

        foreach ($clone as $key => $value) {
            if ($callable($value, $key, $clone)) {
                return $key;
            }
        }

        throw new ElementNotFoundException();
    }

    /**
     * @inheritDoc
     */
    public function findValue(callable $callable): mixed
    {
        return $this[$this->findKey($callable)];
    }

    /**
     * @inheritDoc
     */
    public function keyOf(mixed $value): int|string
    {
        foreach ($this as $k => $v) {
            if ($v === $value) {
                return $k;
            }
        }

        throw new ElementNotFoundException();
    }

    /**
     * @inheritDoc
     */
    public function removeKey(int|string $key): static
    {
        $clone = clone $this;

        if (!$clone->keyExists($key)) {
            throw new BadOffsetException();
        }

        unset($clone[$key]);

        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function removeValue(mixed $value): static
    {
        $clone = clone $this;
        unset($clone[$this->keyOf($value)]);

        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function average(): int|float
    {
        return $this->sum() / $this->count();
    }

    /**
     * @inheritDoc
     */
    public function variance(): int|float
    {
        $avg = $this->average();

        return $this
            ->reduce(
                fn ($sum, $value) => $sum + abs($value - $avg) ** 2,
                0
            )
            /
            $this->count()
        ;
    }

    /**
     * @inheritDoc
     */
    public function standardDeviation(): int|float
    {
        return sqrt($this->variance());
    }

    /**
     * @inheritDoc
     */
    public function quantiles(float $distribution): static
    {
        $quantiles = ExtendedArray
            ::from(
                $this->quantileValues($distribution),
                1
            )
            ->map(fn(ExtendedArrayInterface $group) => $group->first())
            ->slice(1)
        ;

        return static::from($quantiles, $this->depth);
    }

    /**
     * @inheritDoc
     */
    public function quantileKeys(float $distribution): static
    {
        static::validateDistribution($distribution);

        $size = $distribution * $this->count();

        $keys = static
            ::from($this->keys(), 1)
            ->groupBy(fn($value): string => floor($value / $size))
            ->map(fn(ExtendedArrayInterface $group) => $group->first())
            ->slice(1)
        ;

        return static::from($keys, $this->depth);
    }

    /**
     * @inheritDoc
     */
    public function quantileValues(float $distribution): static
    {
        static::validateDistribution($distribution);

        $size = $distribution * $this->count();

        $values = ExtendedArray
            ::from($this, 1)
            ->groupBy(fn($value): string => floor($value / $size))
        ;

        return static::from($values, $this->depth);
    }

    /**
     * @inheritDoc
     * @throws ZeroOrNegativeDepthException
     */
    public function flat(?int $depth = null): static
    {
        if (!static::canDeepen($depth)) {
            return clone $this;
        }

        $flattened = static::from();

        foreach ($this as $key => $value) {
            if ($value instanceof ExtendedArrayInterface) {
                $flattened = $flattened->push(...$value->flat(static::nextDepth($depth)));
            } else {
                $flattened = $flattened->push($value);
            }
        }

        return $flattened;
    }

    /**
     * @inheritDoc
     */
    public function groupBy(callable $grouper): static
    {
        $clone = clone $this;
        $grouper = static::slicedCallableParameters($grouper);
        $grouped = static::from([], $this->depth);

        foreach ($this as $key => $value) {
            $groupKey = $grouper($value, $key, $clone);

            if (!isset($grouped[$groupKey])) {
                $grouped[$groupKey] = [];
            }

            $grouped[$groupKey][] = $value;
        }

        return $grouped;
    }

    /**
     * @inheritDoc
     */
    public function indexBy(callable $indexer): static
    {
        $clone = clone $this;
        $indexer = static::slicedCallableParameters($indexer);
        $indexed = ExtendedArray::from();

        foreach ($this as $key => $value) {
            $indexed[$indexer($value, $key, $clone)] = $value;
        }

        return $indexed;
    }

    /**
     * @inheritDoc
     */
    public function first(): mixed
    {
        $key = $this->keyFirst();

        if ($key !== null) {
            return $this[$key];
        }

        throw new BadOffsetException();
    }

    /**
     * @inheritDoc
     */
    public function last(): mixed
    {
        $key = $this->keyLast();

        if ($key !== null) {
            return $this[$key];
        }

        throw new BadOffsetException();
    }

    /**
     * @inheritDoc
     */
    public function permutation(): static
    {
        $output = static::from();

        $recursivePermutation = static function (self $extendedArray, int $length) use (&$output, &$recursivePermutation): void {
            if ($length === 1) {
                $output = $output->push($extendedArray);
                return;
            }

            $next = $length - 1;
            $recursivePermutation($extendedArray, $next);

            for ($i = 0; $i < $next; $i++) {
                $extendedArray = $extendedArray->swap($i, $next);
                $recursivePermutation($extendedArray, $next);
            }
        };

        $recursivePermutation(
            $this->values(),
            $this->count()
        );

        return $output->sort();
    }

    /**
     * @inheritDoc
     */
    public function fullMap(callable $callable): static
    {
        $clone = clone $this;
        $callable = static::slicedCallableParameters($callable);

        foreach ($clone as $key => $value) {
            $clone[$key] = $callable($value, $key, $clone);
        }

        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function fullReduce(callable $callable, mixed $initial = null): mixed
    {
        $clone = clone $this;
        $carry = $initial;
        $callable = static::slicedCallableParameters($callable);

        foreach ($clone as $key => $item) {
            $carry = $callable($carry, $item, $key, $clone);
        }

        return $carry;
    }

    /**
     * @inheritDoc
     */
    public function fetch(int $position): mixed
    {
        $keys = $this->keys();
        $length = $this->count();

        $keyIndex = $position >= 0
            ? $position
            : $position + $length
        ;

        return $this[$keys[$keyIndex]];
    }

    /**
     * @inheritDoc
     */
    public function swap(int|string $key1, int|string $key2): static
    {
        $clone = clone $this;

        [$clone[$key1], $clone[$key2]] = [$clone[$key2], $clone[$key1]];

        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function with(int|string $key, mixed $value): static
    {
        $clone = clone $this;

        $clone[$key] = $value;

        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function cartesian(ExtendedArrayInterface ...$arrays): static
    {
        $product = $this->map(fn ($item): array => [$item]);

        foreach ($arrays as $array) {
            $temp = static::from();

            foreach ($product as $p) {
                foreach ($array as $item) {
                    $temp[] = [...$p, $item];
                }
            }

            $product = $temp;
        }

        return $product;
    }
}
