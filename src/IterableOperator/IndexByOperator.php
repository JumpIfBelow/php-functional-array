<?php

namespace JumpIfBelow\Arrays\IterableOperator;

/**
 * Operator to transform all keys by using the given operator.
 * It will allow to index using new paradigm.
 */
class IndexByOperator extends AbstractCallableOperator
{
    /**
     * @inheritDoc
     */
    public function transform(iterable $iterable): iterable
    {
        foreach ($iterable as $key => $value) {
            yield $this->callFunction($value, $key, $iterable) => $value;
        }
    }
}
