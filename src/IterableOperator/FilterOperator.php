<?php

namespace JumpIfBelow\Arrays\IterableOperator;

/**
 * Operator using a callable returning a boolean to keep
 * a value or not.
 */
class FilterOperator extends AbstractCallableOperator
{
    /**
     * @inheritDoc
     */
    public function transform(iterable $iterable): iterable
    {
        foreach ($iterable as $key => $value) {
            if (!$this->callFunction($value, $key, $iterable)) {
                continue;
            }

            yield $key => $value;
        }
    }
}
