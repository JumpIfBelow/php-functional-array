<?php

namespace JumpIfBelow\Arrays\IterableOperator;

/**
 * Removes every value already given by the iterator.
 * This might exhaust memory and works well either if the set is small
 * enough or if the values are repeating themselves a lot.
 */
class UniqueOperator implements OperatorInterface
{
    protected function __construct(
        protected readonly bool $strict,
    ) { }

    public static function with(bool $strict): static
    {
        return new static($strict);
    }

    /**
     * @inheritDoc
     */
    public function transform(iterable $iterable): iterable
    {
        $existingValues = [];

        foreach ($iterable as $key => $value) {
            if (in_array($value, $existingValues, $this->strict)) {
                continue;
            }

            $existingValues[] = $value;
            yield $key => $value;
        }
    }
}
