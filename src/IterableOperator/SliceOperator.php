<?php

namespace JumpIfBelow\Arrays\IterableOperator;

/**
 * Takes a given slice of an iterable.
 * Gives a new iterable with only the wanted values.
 */
class SliceOperator implements OperatorInterface
{
    protected function __construct(
        protected readonly int $offset,
        protected readonly ?int $length,
    ) {
    }

    public static function with(int $offset, ?int $length): static
    {
        return new static($offset, $length);
    }

    /**
     * @inheritDoc
     */
    public function transform(iterable $iterable): iterable
    {
        $currentOffset = 0;
        $sent = 0;

        foreach ($iterable as $key => $value) {
            if ($currentOffset++ < $this->offset) {
                continue;
            }

            if ($this->length !== null && $sent >= $this->length) {
                break;
            }

            $sent++;
            yield $key => $value;
        }
    }
}
