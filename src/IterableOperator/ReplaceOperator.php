<?php

namespace JumpIfBelow\Arrays\IterableOperator;

/**
 * Replaces each value of an iterable with a given value.
 * Allows to strictly check for equality or loosely.
 */
class ReplaceOperator implements OperatorInterface
{
    protected function __construct(
        protected readonly mixed $search,
        protected readonly mixed $replace,
        protected readonly bool $strict,
    ) {
    }

    public static function with(
        mixed $search,
        mixed $replace,
        bool $strict = true,
    ): static {
        return new static($search, $replace, $strict);
    }

    /**
     * @inheritDoc
     */
    function transform(iterable $iterable): iterable
    {
        $checker = $this->strict
            ? fn (mixed $value): bool => $value === $this->search
            : fn (mixed $value): bool => $value == $this->search
        ;

        foreach ($iterable as $key => $value) {
            if ($checker($value)) {
                yield $key => $this->replace;
                continue;
            }

            yield $key => $value;
        }
    }
}
