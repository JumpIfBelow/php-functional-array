<?php

namespace JumpIfBelow\Arrays\IterableOperator;

use JumpIfBelow\Arrays\Traits\CallableParametersTrait;

/**
 * AbstractCallableOperator class is an abstract class to implement
 * {@see OperatorInterface} more easily. The function may accept
 * all parameters $value, $key and $iterable.
 */
abstract class AbstractCallableOperator implements OperatorInterface
{
    use CallableParametersTrait;

    protected function __construct(
        protected readonly \Closure $closure,
    ) {
    }

    /**
     * Easily instanciate an operator with a callable.
     * The purpose is to drop any extra parameters if needed, as calling a native PHP function
     * might not work as expected in those cases.
     * @param callable $callable The callable to use for the operator to work.
     * @return static A new instance of the operator.
     */
    public static function with(callable $callable): static
    {
        return new static(
            static::slicedCallableParameters($callable)(...),
        );
    }

    /**
     * Call the callable with the expected parameters to work with.
     */
    protected function callFunction(mixed $value, mixed $key, iterable $iterable): mixed
    {
        return ($this->closure)($value, $key, $iterable);
    }
}
