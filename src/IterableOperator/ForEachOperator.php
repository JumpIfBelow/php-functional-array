<?php

namespace JumpIfBelow\Arrays\IterableOperator;

/**
 * Operator to call the given callable on each item.
 * Returns the same iterator, as-is.
 */
class ForEachOperator extends AbstractCallableOperator
{
    /**
     * @inheritDoc
     */
    public function transform(iterable $iterable): iterable
    {
        foreach ($iterable as $key => $value) {
            $this->callFunction($value, $key, $iterable);
        }

        return $iterable;
    }
}
