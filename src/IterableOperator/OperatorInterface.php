<?php

namespace JumpIfBelow\Arrays\IterableOperator;

/**
 * Defines how an Operator should work.
 */
interface OperatorInterface
{
    /**
     * Takes an iterable as an input and returns a new one.
     * @param iterable $iterable The iterable to transform.
     * @return iterable The transformed iterable.
     */
    function transform(iterable $iterable): iterable;
}
