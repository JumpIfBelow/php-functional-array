<?php

namespace JumpIfBelow\Arrays\IterableOperator;

/**
 * Transforms each value of the iterable using a callable.
 * Returns a new iterable with the changed values.
 */
class MapOperator extends AbstractCallableOperator
{
    /**
     * @inheritDoc
     */
    public function transform(iterable $iterable): iterable
    {
        foreach ($iterable as $key => $value) {
            yield $key => $this->callFunction($value, $key, $iterable);
        }
    }
}
