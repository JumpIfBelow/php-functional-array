<?php

namespace JumpIfBelow\Arrays;

class Entry
{
    public function __construct(
        protected readonly mixed $key,
        protected readonly mixed $value,
    ) { }

    public function getKey(): mixed
    {
        return $this->key;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }
}
