<?php

namespace JumpIfBelow\Arrays;

use JumpIfBelow\Arrays\Exception\BadOffsetException;
use JumpIfBelow\Arrays\Exception\ElementNotFoundException;

interface ExtendedArrayInterface extends FluentArrayInterface
{
    /**
     * @param callable $callable fn($value, $key, $this)
     * @return static
     */
    public function forEach(callable $callable): static;

    /**
     * @param callable $callable fn($value, $key, $this): bool
     * @return bool True if all values validate the function, false otherwise.
     */
    public function every(callable $callable): bool;

    /**
     * @param callable $callable fn($value, $key, $this): bool
     * @return bool True if one value validates the function, false otherwise.
     */
    public function some(callable $callable): bool;

    /**
     * Gives the current entry the pointer is set to.
     * @return ?Entry The entry if there is any, null if the pointer is at the end.
     * @see current() Same as deprecated current(), but return an object instead.
     */
    public function entry(): ?Entry;

    /**
     * Advance internal pointer and returns the key.
     * @return int|string|null The key the pointer is set to. Null if there is no longer new keys.
     */
    public function nextKey(): int|string|null;

    /**
     * Advance internal pointer and returns the value.
     * @return mixed|null The value the pointer is set to. Null if there is no longer new values.
     */
    public function nextValue(): mixed;

    /**
     * Advance internal pointer and returns the whole entry.
     * @return ?Entry The entry the pointer is set to. Null if there is no longer new entries.
     */
    public function nextEntry(): ?Entry;

    /**
     * @param callable $callable fn($value, $key, $this): bool
     * @return int|string
     * @throws ElementNotFoundException
     */
    public function findKey(callable $callable): int|string;

    /**
     * @param callable $callable fn($value, $key, $this): bool
     * @return mixed
     * @throws ElementNotFoundException
     */
    public function findValue(callable $callable): mixed;

    /**
     * Returns the key of the array containing the given value.
     * @param mixed $value
     * @return int|string
     * @throws ElementNotFoundException
     */
    public function keyOf(mixed $value): int|string;

    /**
     * Removes the given key from the array.
     * @param int|string $key The key to remove.
     * @return static
     * @throws BadOffsetException
     */
    public function removeKey(int|string $key): static;

    /**
     * Removed the first matching value.
     * @param mixed $value The element to be removed.
     * @return static
     * @throws ElementNotFoundException
     */
    public function removeValue(mixed $value): static;

    /**
     * @return int|float
     */
    public function average(): int|float;

    /**
     * @return int|float
     */
    public function variance(): int|float;

    /**
     * @return int|float
     */
    public function standardDeviation(): int|float;

    /**
     * <b>Warning</b>: works only on sorted array, fuzzy otherwise.
     * @param float $distribution
     * @return static
     */
    public function quantiles(float $distribution): static;

    /**
     * <b>Warning</b>: works only on sorted array, fuzzy otherwise.
     * @param float $distribution Number between 0 and 1.
     * @return static
     */
    public function quantileKeys(float $distribution): static;

    /**
     * <b>Warning</b>: works only on sorted array, fuzzy otherwise.
     * @param float $distribution Number between 0 and 1.
     * @return static
     */
    public function quantileValues(float $distribution): static;

    /**
     * Flatten the array with the given deepness.
     * @param int|null $depth
     * @return static
     */
    public function flat(?int $depth = null): static;

    /**
     * Groups values using a grouper function. It will test each value to know if it belongs to an existing group.
     * If none are matched, a new one is created.
     * @param callable $grouper fn($value, $key, $this): string|int
     * @return static
     */
    public function groupBy(callable $grouper): static;

    /**
     * Indexes values using an indexer function. It will use it to get an actual key to retain the value.
     * Know that if the indexer returns the same value several times, only the last item will remain.
     * @param callable $indexer fn($value, $key, $this): string|int
     * @return static
     */
    public function indexBy(callable $indexer): static;

    /**
     * Get the first element of an array.
     * @return mixed
     * @throws BadOffsetException
     */
    public function first(): mixed;

    /**
     * Get the last element of an array.
     * @return mixed
     * @throws BadOffsetException
     */
    public function last(): mixed;

    /**
     * Find all of the permutations of the given length.
     * @return static
     */
    public function permutation(): static;

    /**
     * Performs a map which accepts only one callable with the value, key and actual array inside.
     * @param callable $callable fn($value, $key, $this): mixed
     * @return static
     */
    public function fullMap(callable $callable): static;

    /**
     * Performs a map which accepts only one callable with the value, key and actual array inside.
     * @param callable $callable fn($value, $key, $this): mixed
     * @param mixed $initial The initial value to begin with.
     * @return mixed
     */
    public function fullReduce(callable $callable, mixed $initial = null): mixed;

    /**
     * Returns the element at given position. If a negative number is supplied, starts from the end.
     * If the array contains string or is not contiguous, only the order of the elements matters.
     * @param int $position The position to fetch.
     * @return mixed
     */
    public function fetch(int $position): mixed;

    /**
     * @param int|string $key1
     * @param int|string $key2
     * @return static
     */
    public function swap(int|string $key1, int|string $key2): static;

    /**
     * @param int|string $key
     * @param mixed $value
     * @return static
     */
    public function with(int|string $key, mixed $value): static;

    /**
     * @param self ...$arrays
     * @return static
     */
    public function cartesian(self ...$arrays): static;
}
